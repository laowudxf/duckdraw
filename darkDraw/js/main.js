import './Extension/WxObjectExtension'
import * as ViewKit from './libs/ViewKit/index'
import HomeView from './Views/HomeView'
import HomeViewController from './Views/VC/HomeViewController'
import Test from './Views/Test'
import DataBus from './DataBus'
import Network from './Helper/Network'
import EndPoint from './Helper/EndPoint'
import ShareHelper from './Helper/ShareHelper'

import CheckPointView from './Views/CheckPointView'
export default class Main {
  constructor() {
    EndPoint.config()
    .then(data => {
      if (data.data.data) {
        let config = JSON.parse(data.data.data)
        console.log(config);
        DataBus.share().server_config = config
      }
    })
    .then(() => EndPoint.share_content())
    .then((data) => {
        DataBus.share().share_content = data.data.data
        console.log(DataBus.share().share_content, 333333333333);
    })
    .then(() => DataBus.share().serverLogin())

    this.app = new ViewKit.App({canvas})
    if (DataBus.share().isDebug) {
      Network.config.baseUrl = 'http://caige-app.dev.max06.com/'
    }

    this.setup()
    this.setupUI()

    // EndPoint.share_content()
    // .then((data) => {
    //     console.log(data, 333333333333);
    // })
  }

  setup() {

    wx.showShareMenu({
      withShareTicket: true
    })

    wx.onShow( data => {
      console.log('onShow:', data);
      DataBus.share().shareTicket = data.shareTicket
      DataBus.share().onShowBlocks.map(x => {
        x()
      })
      DataBus.share().onShowBlocks.length = 0

    })

    wx.onShareAppMessage(() => {
      return ShareHelper.shareInfo()
    })

    wx.onHide( data => {
       console.log('onHide', data);
      DataBus.share().onHideBlocks.map(x => {
        x()
      })
      DataBus.share().onHideBlocks.length = 0
    })

    ViewKit.Button.soundName = 'button'
  }

  setupUI() {

    sharedCanvas.width = canvas.width
    sharedCanvas.height = canvas.height
    DataBus.postMessage({method: 'shouldReDraw'})

    let homeVC = new HomeViewController()
    let nav = new ViewKit.NavigationController(homeVC)
    this.app.window.rootViewController = nav

  }
}


import User from '../DataModel/User'
import DataBus from '../DataBus'
import ShareHelper from '../Helper/ShareHelper'

let serviceShareInstance

export default class Service {
  constructor() {
    if (serviceShareInstance) {
      return serviceShareInstance
    }
    this.user = User.share()
    serviceShareInstance = this
  }

  static share() {
    return new Service()
  }


  //通关
  // cat_id => group的id
  passComplete(cat_id, index) {
    if (DataBus.share().isValidPassComplete(cat_id, index) == false) {
      return false
    }


    let next = DataBus.share().nextCatModelWithId(cat_id, index)
    if (next) {
      this.user.gameProcessPosition = {cat_id: next.model.id, index: next.index}
    }

    //set highestScore
    let countData = DataBus.share().catModelWithId(cat_id, index)
    let score = countData.count + index + 1
    DataBus.postMessage({
      method: 'saveHighestScore',
      param: {
        score: score
      }
    })
    // ------------
    return true
  }

  tip() {
    if (this.user.coin < 50) {
      return -1
    }

    this.user.coin -= 50
    return 0
  }

}

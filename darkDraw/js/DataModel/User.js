import * as ViewKit from '../libs/ViewKit/index'
import EndPoint from '../Helper/EndPoint'
import DataBus from '../DataBus'

export default class User extends ViewKit.Observable {
  constructor() {
    super()
    if (User.shareInstance) {
      return User.shareInstance
    }

    User.shareInstance = this
    try {
      this.passPaths = ViewKit.Storage.get('passPath')
    } catch (e) {
      this.passPaths = {}
    }
  }

  get coin () {
    return this._coin
  }

  set coin(value) {
    let offset = value - this._coin
    this.saveCoin(offset)
    this._coin = value
    this.notice(value, 'coin')
  }

  get gameProcessPosition () {
    return this._gameProcessPosition
  }

  //弃用
  get userValuePass() {
    return this.userInfo.values.pass
  }

  set gameProcessPosition(value) {
    this._gameProcessPosition = value
    this.saveOptionValue()
    //save pass
    let pass = this.selfScore
    if (pass != null && this.userValuePass != null) {
      EndPoint.modifyUserInfo('pass', pass - this.userValuePass)
    }
    this.notice(value, 'gameProcessPosition')
  }

  //passData 没有加载之前 无法计算
  get selfScore() {
    if (this.gameProcessPosition) {
      let r = DataBus.share().catModelWithId(this.gameProcessPosition.cat_id)
      if (r == null) {
        return null
        // return this.userValuePass
      }
      return (r.count + this.gameProcessPosition.index)
    } else {
      return null
    }
  }

  get passPaths () {
    let v = this._passPaths
    if (v == null) {
      return {}
    }
    return v
  }

  set passPaths(value) {
    if (value == null) {
      value = {}
    }
    this._passPaths = value
    this.savePassPaths()
  }

  savePassPaths() {
    ViewKit.Storage.set('passPath', this.passPaths)
  }

  setupWithOption(optionValue) {
    this._gameProcessPosition = optionValue.gameProcessPosition
  }

  static share() {
    return new User()
  }

  get userInfo () {
    return this._userInfo
  }

  set userInfo(value) {
    this._userInfo = value
    this.coin = value.values.gold

    if (value.values.option) {
      let option = JSON.parse(value.values.option)
      this.setupWithOption(option)
    }

    this.notice(value, 'userInfo')
    // this.coin = 1000
  }


  saveCoin(addOrSubCoin) {
    return EndPoint.modifyUserInfo('gold', addOrSubCoin)
  }


  saveOptionValue() {
    return EndPoint.modifyUserInfo('option', JSON.stringify({gameProcessPosition: this.gameProcessPosition}))
  }
}

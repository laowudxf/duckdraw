
export default class SoundHelper {
  static playSoundWithName(name, finish) {
    if (this.cache == null) {
      this.cache = {}
    }

    if (this.openVoice == false) {
      if(finish) {
        finish()
      }
      return
    }

    let audio = this.cache['name']

    if (!audio) {
      audio = wx['createInnerAudioContext']()
      audio.src = 'audio/' + name + '.mp3' // src 可以设置 http(s) 的路径，本地文件路径或者代码包文件路径
      audio.onError(err => {
        console.log('sound err:', err);
      })
    }

    //cache
    audio.play()
    if (finish) {
      audio.onEnded(() => {
        finish()
      })
    }
  }
}

SoundHelper.openVoice = true

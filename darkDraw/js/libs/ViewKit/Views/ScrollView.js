import {Rect, Point, Size} from '../util'
import Device from '../Helper/Device'
import View from './View'
import * as Gesture from '../GestureRecognzer/GestureRecognzer'

export default class ScrollView extends View {
  setupUI() {
    //阻力系数
    this.lastOffset = 0
    this.dragCoefficient = 0.5 / 60
    this.maskToBounds = true
    this.beginDecelerating = false
    console.log(this.bounds);
    this.contentView = new View(this.bounds)
    this.addSubview(this.contentView)
    this.tmpContentOffset = this.contentOffset

    let ges = new Gesture.PanGestureRecognzer(this, (e) => {
      if (e.state == 3) {
        this.tmpContentOffset = this.contentOffset
        this.beginDecelerating = true
      } else if (e.state == 2) {
        let offset = e.currentPoint.y - e.startPoint.y
        this.lastOffset = offset
        this.contentOffset = new Point(this.contentOffset.x, this.tmpContentOffset.y + offset)
      }
    })
    this.addGesture(ges)
    console.log(this.gestures);
  }

  draw(ctx, rect) {
    // if (this.beginDecelerating) {
    //   let tmp = this.lastOffset * this.dragCoefficient
    //   console.log(2222222222, tmp);
    //     this.contentOffset = new Point(this.contentOffset.x, this.contentOffset.y += tmp)
    //   this.lastOffset = this.lastOffset - tmp
    //   console.log(this.lastOffset);
    //   if (tmp < 2 && tmp > -2) {
    //     this.beginDecelerating = false
    //     this.tmpContentOffset = this.contentOffset
    //   }
    // }
    super.draw(ctx, rect)
  }

  set contentSize(value) {
    this.contentView.rect.size = value
  }

  get contentSize() {
    return this.contentView.rect.size
  }

  set contentOffset(value) {
    if (-value.y + this.height > this.contentView.height) {
      value.y = Math.floor(this.height - this.contentView.height)
    }
    if (value.y > 0) {
      value.y = 0
    }
    this.contentView.x = value.x
    this.contentView.y = value.y
  }

  get contentOffset() {
    return new Point(this.contentView.x, this.contentView.y)
  }

  touchEvent(e) {
    super.touchEvent(e)
  }


}

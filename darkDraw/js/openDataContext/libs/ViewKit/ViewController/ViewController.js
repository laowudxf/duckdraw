import {Rect, Point, Size} from '../util'
import Device from '../Helper/Device'
import View from '../Views/View'

export default class ViewController {
  constructor() {
    this.screenRect = Rect.rect(0, 0, Device.share().screenWidth, Device.share().screenHeight)
    this.viewControllers = []
    this.parentContainer = null

  }

  get currentViewController() {
    if (this.viewControllers.length == 0) {
      return 0
    }

    return this.viewControllers[this.viewControllers.length - 1]
  }

  loadView() {
    this.view = new View(Rect.rect(0, 0, Device.share().screenWidth, Device.share().screenHeight))
  }

  get view () {
    if (this._view == null) {
      this.loadView()
      this.viewDidLoad()
    }
    return this._view
  }

  set view(value) {
    this._view = value
  }

  viewDidLoad() {
    this.setupUI()
    this.addActionTarget()
  }

  setupUI() {}
  addActionTarget() {}

  viewWillAppear() {}
  viewWillDisappear() {}
  viewDidAppear() {}
  viewDidDisappear() {}


  addChild(viewController) {
    if (viewController == null) {
      return
    }

    if (this.currentViewController) {
      this.currentViewController.viewWillDisappear()
      this.currentViewController.view.removeFromParent()
    }
    viewController.viewWillAppear()
    this.viewControllers.push(viewController)
    viewController.parentContainer = this
    this.view.addSubview(viewController.view)

    if (this.currentViewController) {
      this.currentViewController.viewDidDisappear()
    }

    viewController.viewDidDisappear()
  }

  removeChild(viewController) {
    if (viewController == null) {
      return
    }

    let index = this.viewControllers.indexOf(viewController)
    if (index == - 1) {
      return
    }

    let isCurrentVC = viewController == this.currentViewController

    if (index >= 0) {
      viewController.viewWillDisappear()
      if (isCurrentVC && index > 0) {
        (this.viewControllers[index - 1]).viewWillAppear()
      }
      viewController.parentContainer = null
      this.viewControllers.splice(index, 1)
      viewController.view.removeFromParent()
      viewController.removeFromParent()
      viewController.viewDidDisappear()

      if (isCurrentVC && index > 0) {
        this.view.addSubview((this.viewControllers[index - 1]).view)
        let vc = this.viewControllers[index - 1]
        vc.viewDidAppear()
      }
    }
  }

  removeFromParent() {
    if (this.parentContainer) {
      this.parentContainer.removeChild(this)
    }
  }
}



let systemInfo = {
  info: wx.getSystemInfoSync(),
  is_x (){
    return this.info.statusBarHeight == 44
  },

  topHeight () {
    console.log(this.info);
    // return this.is_x ? 44 : 30
    return this.info.statusBarHeight
  }
}

let screenScale = systemInfo.info.pixelRatio

class Point {
  constructor(x = 0, y = 0) {
    this.x = x
    this.y = y
  }

  static zero() {
      return  new Point();
  }

  plusPoint(point) {
    return new Point(this.x + point.x, this.y + point.y)
  }
}

class Size {
  constructor(width = 0, height = 0) {
    this.width = width
    this.height = height
  }

  static zero() {
      return  new Size();
  }

  static ScreenSize() {
    return new Size(canvas.width, canvas.height)
  }

  static originScreenSize() {
    return new Size(canvas.width / screenScale, canvas.height / screenScale)
  }
}

class Rect {
  constructor(point = new Point(), size = new Size() ) {
    this.point = point
    this.size = size
  }

  static zero() {
      return  new Rect();
  }

  copy() {
    return Rect.rect(this.x, this.y, this.width, this.height)
  }
  //getter
  get bounds() {
    return Rect.rect(0, 0, this.width, this.height)
  }

  get width() {
    return this.size.width
  }

  get height() {
    return this.size.height
  }

  get x() {
    return this.point.x
  }

  get y() {
    return this.point.y
  }
  //setter
  set width (value) {
      this.size.width = value
  }

  set height (value) {
      this.size.height = value
  }

  set x (value) {
    this.point.x = value
  }

  set y (value) {
    this.point.y = value
  }


  get center() {
    return new Point(this.width / 2 + this.x, this.height / 2 + this.y)
  }

  rectArr() {
    return [this.x, this.y, this.width, this.height]
  }

  get bottom () {
    return this.y + this.height
  }

  get right () {
    return this.x + this.width

  }

  simpleRect() {
    return {x: this.x, y: this.y, width: this.width, height: this.height}
  }


  static screenRect() {
    return new Rect(Point.zero(), Size.ScreenSize())
  }

  static rect(x, y, width, height) {
    return new Rect(new Point(x, y), new Size(width, height))
  }

  rectInRect(rect) {
    return new Rect(this.point.plusPoint(rect.point), this.size)
  }

  containPoint(point) {
    return this.point.x <= point.x && this.point.y <= point.y &&
     this.size.width >= (point.x - this.point.x) && this.size.height >= (point.y - this.point.y)
  }

  equal(rect) {
    return (this.x == rect.x && this.y == rect.y && this.width == rect.width && this.height == rect.height)
  }
}

function drawUsingArc(rect, r, ctx) {
  let x = rect.x
  let y = rect.y
  let w = rect.width
  let h = rect.height

  if (w < 2 * r) r = w / 2;
  if (h < 2 * r) r = h / 2;
  ctx.beginPath();
  ctx.moveTo(x+r, y);
  ctx.arcTo(x+w, y, x+w, y+h, r);
  ctx.arcTo(x+w, y+h, x, y+h, r);
  ctx.arcTo(x, y+h, x, y, r);
  ctx.arcTo(x, y, x+w, y, r);
  // ctx.arcTo(x+r, y);
  ctx.closePath();
}

function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min)) + min; //The maximum is exclusive and the minimum is inclusive
}

export {Point, Size, Rect, drawUsingArc, getRandomInt, screenScale, systemInfo}


import * as ViewKit from '../libs/ViewKit/index'
import DataBus from '../DataBus'
import Fireworks from './Particle/Fireworks'

export default class PreAfterView extends ViewKit.View {

  get preUser () {
    if (this._preUser == null) {
      let v = null
      v = new ViewKit.ImageView(null, ViewKit.Rect.rect(0, 0, 56, 56))
      v.anchorY = 1
      v.position = new ViewKit.Point(this.width / 2 - 100, this.meView.rect.bottom)
      v.cornerRadius = v.height / 2
      v.maskToBounds = true
      this._preUser = v
    }
    return this._preUser
  }

  get meView () {
    if (this._meView == null) {
      let v = null
      v = new ViewKit.ImageView(null, ViewKit.Rect.rect(0, 0, 64, 64))
      v.anchorY = 0
      v.position = new ViewKit.Point(this.width / 2, ViewKit.Device.share().isPhoneX ? 140 + 40 : 140)
      v.cornerRadius = v.height / 2
      v.maskToBounds = true
      this._meView = v
    }
    return this._meView
  }

  get meLabel () {
    if (this._meLabel == null) {
      let v = null
      v = new ViewKit.Label(ViewKit.Rect.rect(0, 0, 60, 20))
      v.textAlign = 'center'
      v.textColor = 'white'
      v.bgColor = '#e76441'
      v.text = ''
      v.anchorY = 0
      v.setFont(13)
      v.cornerRadius = 3
      v.position = new ViewKit.Point(this.width / 2, this.meView.rect.bottom + 10)
      this._meLabel = v
    }
    return this._meLabel
  }

  get preLabel () {
    if (this._preLabel == null) {
      let v = null
      v = new ViewKit.Label(ViewKit.Rect.rect(0, 0, 60, 20))
      v.textAlign = 'center'
      v.textColor = '#e76441'
      v.text = '等待挑战'
      v.anchorY = 0
      v.setFont(13)
      v.position = new ViewKit.Point(this.preUser.center.x, this.preUser.rect.bottom + 10)
      this._preLabel = v
    }
    return this._preLabel
  }

  get lastLabel () {
    if (this._lastLabel == null) {
      let v = null
      v = new ViewKit.Label(ViewKit.Rect.rect(0, 0, 60, 20))
      v.textAlign = 'center'
      v.textColor = '#e76441'
      v.text = ''
      v.anchorY = 0
      v.setFont(13)
      v.position = new ViewKit.Point(this.lastUser.center.x, this.lastUser.rect.bottom + 10)
      this._lastLabel = v
    }
    return this._lastLabel
  }

  get lastUser () {
    if (this._lastUser == null) {
      let v = null
      v = new ViewKit.ImageView(null, ViewKit.Rect.rect(0, 0, 56, 56))
      v.anchorY = 1
      v.position = new ViewKit.Point(this.width / 2 + 100, this.meView.rect.bottom)
      v.cornerRadius = v.height / 2
      v.maskToBounds = true
      this._lastUser = v
    }
    return this._lastUser
  }

  setupUI() {
    let views = [this.meView, this.preUser, this.lastUser, this.meLabel, this.preLabel, this.lastLabel]
    views.map(x => {
        this.addSubview(x)
    })
  }


  setupData() {
    DataBus.share().getBehandAfterInfo(DataBus.share().openId, DataBus.share().currendScore)
    .then(data => {
        let self = data[1]
        this.meView.imageSrc = self.avatarUrl
        let selfValue = self.KVDataList[0].value
        this.meLabel.text = '第' + selfValue + '关'

        let pre = data[0]
        let after = data[2]
        if (pre) {
            this.preUser.imageSrc = pre.avatarUrl
            this.preLabel.isHidden = false
        } else {
          this.preLabel.isHidden = true
        }

        if (after) {
            this.lastUser.imageSrc = after.avatarUrl
            let value = after.KVDataList[0].value
            this.lastLabel.text = '第' + value + '关'
            this.lastUser.isHidden = false
            this.lastLabel.isHidden = false
        } else {
            this.lastUser.isHidden = true
            this.lastLabel.isHidden = true
        }
    })

  }

  refresh() {
    this.setupData()
    this.setupFirework()
  }

  setupFirework() {
    let v = new Fireworks(ViewKit.Rect.rect(0, 0, 10, 10))
    v.position = new ViewKit.Point(this.width / 2 - 50, 150)
    this.addSubview(v)

    setTimeout(() => {
      let v1 = new Fireworks(ViewKit.Rect.rect(0, 0, 10, 10))
      v1.position = new ViewKit.Point(this.width / 2, 150)
      this.addSubview(v1)

    }, 100)

    setTimeout(() => {
      let v2 = new Fireworks(ViewKit.Rect.rect(0, 0, 10, 10))
      v2.position = new ViewKit.Point(this.width / 2 + 50, 150)
      this.addSubview(v2)

    }, 200)

  }
}

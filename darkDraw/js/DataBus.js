import EndPoint from './Helper/EndPoint'
import User from './DataModel/User'
import * as ViewKit from './libs/ViewKit/index'

import * as config from './config'

export default class DataBus {

  static share() {
    return new DataBus()
  }

  static postMessage(data) {
    const openDataContext = wx.getOpenDataContext();
    openDataContext.postMessage(data);
  }

  get lastGetDailyRewardTime() {
    let r = null
    try {
      r = ViewKit.Storage.get('lastGetDailyRewardTime')
    } catch(err) {
      return null
    }
    return r
  }

  set lastGetDailyRewardTime(time) {
    ViewKit.Storage.set('lastGetDailyRewardTime', time)
  }

  get openVoice () {
    return this._openVoice
  }

  set openVoice(value) {
    this._openVoice = value
    ViewKit.SoundHelper.openVoice = value
  }

  get preProduct() {
    let build = this.server_config.build
    if (build != null) {
      if (this.config.build >= build && this.server_config.preproduct) {
        return true
      }
    }
    return false
  }

  constructor() {
    if (DataBus.shareInstance) {
      return DataBus.shareInstance
    }

    DataBus.shareInstance = this
    this.config = config.config
    this.openVoice = true
    this.isDebug = true
    this.isDebug = false
    this.passData = null

    //callbacks
    this.onShowBlocks = []
    this.onHideBlocks = []

  }

  get passData () {
    return this._passData
  }

  set passData(value) {
    this._passData = value
    let gameProcessPosition = User.share().gameProcessPosition
    if (value && gameProcessPosition == null) {
      User.share().gameProcessPosition = {
        cat_id: value[0].info[0].id,
        index: 0
      }
    }
  }

  serverLogin() {
    return EndPoint.login()
    .then((data) => {
      console.log('userinfo:', data.data);
      User.share().userInfo = data.data.data
      this.userInfo = User.share().userInfo
      DataBus.postMessage({
        method: 'setOpenId',
        param: {openId: this.userInfo.openid}
      })
      return data.data.data
    })
    .then(() => {
      EndPoint.pass()
      .then(data => {
        DataBus.share().passData = data.data.data
        //为了计算已经通过记录
        User.share().userInfo = User.share().userInfo
      })
    })
    .catch(err => {
      console.log(err);
    })
  }

  catModelWithId(id) {
    if (this.passData == null) {
      return null
    }
    let count = 0
    for (let i = 0; i < this.passData.length ; i++) {
      let info = this.passData[i].info
      for(let j = 0; j < info.length; j++ ) {
        if (info[j].id == id) {
          return  {
            model: info[j],
            i,
            j,
            count
          }
        }
        count += info[j].pass.length
      }
    }
    return null
  }

  nextCatModelWithId(id, index) {
    let model = this.catModelWithId(id)
    if (index == (model.model.pass.length - 1)) { //last
      if ((this.passData[model.i].info.length - 1) == model.j) { //last pass group
        if ((this.passData.length - 1) == model.i) { //last pass
          return null
        }
        model.i += 1
        model.j = 0
        model.model = this.passData[model.i].info[model.j]
      } else {
        model.j += 1
        model.model = this.passData[model.i].info[model.j]
      }
      model.index = 0

    } else {
      model.index = index + 1
    }
    model.count += 1
    return model
  }

  passIndexInt(id) {
    return this.catModelWithId(id).count
  }

  isPassGroupLock(id) {
    let gameProcessPosition = User.share().gameProcessPosition
    if (gameProcessPosition) {
      return gameProcessPosition.cat_id < id
    } else {
      return false
    }
  }

  isPassGroupPassLock(id, index) {
    let gameProcessPosition = User.share().gameProcessPosition
    if (gameProcessPosition) {
      if (gameProcessPosition.cat_id <= id && gameProcessPosition.index <= index) {
        if (gameProcessPosition.cat_id == id && gameProcessPosition.index == index) { //当前关卡
            return 0
        } else { //未通关
          return -1
        }
      } else { //通过
          return 1
      }
      // return gameProcessPosition.cat_id <= id && gameProcessPosition.index < index
    } else {
      console.log('gameProcessPosition invalid: is empty');
      return -1
    }
  }

  isValidPassComplete(cat_id, index) {
    let gameProcessPosition = User.share().gameProcessPosition
    if (gameProcessPosition) {
      if (gameProcessPosition.cat_id > cat_id) {
        return false
      }

      if (gameProcessPosition.index > index) {
        return false
      }
    }

    return true
  }

  passPathWith(id, index) {
    let passPath = User.share().passPaths[id + '_' + index]
    return passPath
  }

  showChaseView() {
    DataBus.postMessage({
      method: 'setRootView',
      param: {
        index: 1
      }
    })

    this.showOpenDataView()
  }

  showRankBoard() {
    DataBus.postMessage({
      method: 'setRootView',
      param: {
        index: 0
      }
    })

    this.showOpenDataView()
  }

  hiddenBoard() {
    this.showOpenDataView(false)
  }

  showPreAfterView(score = 1) {
    DataBus.postMessage({
      method: 'setRootView',
      param: {
        index: 2,
        score: score
      }
    })

    this.showOpenDataView()
  }

  hiddenPreAfterView() {
    this.showOpenDataView(false)
  }

  showOpenDataView(show = true) {
    if (show) {
      DataBus.postMessage({
        method: 'shouldRefresh',
        param: true
      })
      ViewKit.App.share().window.addSubCanvas(sharedCanvas)

    } else {
      DataBus.postMessage({
        method: 'shouldRefresh',
        param: false
      })
      ViewKit.App.share().window.removeSubCanvas(sharedCanvas)

    }
  }

}

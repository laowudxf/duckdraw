import * as ViewKit from '../libs/ViewKit/index'
import DataBus from '../DataBus'

export default class ShareHelper {

  static share(validTime = 4) {

    wx.aldSendEvent("分享")

    let share_content = DataBus.share().share_content.concat()
    console.log(share_content);
    if (DataBus.share().preProduct == false) {
      share_content.shift()
    }
    if (share_content.length == 0) {
      return
    }
    let r_index = this.getRandomInt(share_content.length)
    if (DataBus.share().preProduct) {
        r_index = 0
    }
    let share_d = share_content[r_index]


    if (validTime) {
      DataBus.share().shareTime = Date.now()
    }

    let p = new Promise((s, f) => {
      if (validTime) {
        DataBus.share().onShowBlocks.push(() => {
          let offset = (Date.now() - DataBus.share().shareTime)
          console.log('shareTime:', DataBus.share().shareTime);
          console.log('offset:', offset);
          console.log('validTime:', validTime);
          if (offset > (validTime * 1000)) {
            s()
          } else {
            f()
          }
        })
      } else {
        s()
      }
    })

    wx.shareAppMessage({
      title: share_d.title,
      imageUrl: share_d.pic
    })

    return p
  }

  static shareInfo() {

    let share_content = DataBus.share().share_content.concat()
    if (DataBus.share().preProduct == false) {
      share_content.shift()
    }

    if (share_content.length == 0) {
      return
    }

    let r_index = this.getRandomInt(share_content.length)
    if (DataBus.share().preProduct) {
        r_index = 0
    }
    let share_d = share_content[r_index]


    return {
      title: share_d.title,
      imageUrl: share_d.pic
    }
    // return {title: info[0], imageUrl:'https://ss1.bdstatic.com/70cFvXSh_Q1YnxGkpoWK1HF6hhy/it/u=2735633715,2749454924&fm=27&gp=0.jpg'}

  }

  static getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
  }
}

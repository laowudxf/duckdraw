
import * as ViewKit from '../libs/ViewKit/index'

export default class Test extends ViewKit.View{

  get button () {
    if (this._button == null) {
      this._button = new ViewKit.Button(ViewKit.Rect.rect(0, 0, 100, 100))
      this._button.position = new ViewKit.Point(this.width / 2, this.height /  2)
    }
    return this._button
  }


  setupUI() {
    this.bgColor = 'red'
    this.addSubview(this.button)

    setInterval(() => {
      this.button.x += 1
    }, 16)
  }
}

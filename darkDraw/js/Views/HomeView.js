import * as ViewKit from '../libs/ViewKit/index'
import CheckPointView from './CheckPointView'
import RankBoard from './RankBoard/RankBoard'
import User from '../DataModel/User'
import ShareHelper from '../Helper/ShareHelper'
import DataBus from '../DataBus'
import RandomJumpGameView from './Home/RandomJumpGameView'
import DuckHead from './Home/DuckHead'

export default class HomeView extends ViewKit.View {

  get bgImageView () {
    if (this._bgImageView == null) {
      let v = null
      v = new ViewKit.ImageView(ViewKit.getRes('bgImage'), this.bounds)
      v.width += 50
      v.height += 50
      v.anchorX = 0
      v.anchorY = 1
      v.alpha = 0.3
      v.position = new ViewKit.Point(0, this.height)
      v.imageMode = 'repeat'
      this._bgImageView = v
    }
    return this._bgImageView
  }

  get play_top () {
    if (this._play_top == null) {
      let v = null
      v = new ViewKit.ImageView(ViewKit.getRes('play_top'), ViewKit.Rect.rect(0, 0 , 207, 77))
      v.position = this.playBtn.bounds.center
      this._play_top = v
    }
    return this._play_top
  }

  get giftBtn () {
    if (this._giftBtn == null) {
      let v = null
      v = new ViewKit.Button(ViewKit.Rect.rect(0, 0, 30, 53.5))
      v.bgImage = ViewKit.getRes('gift')
      v.anchorX = 0
      v.anchorY = 0
      v.position = new ViewKit.Point(this.duck_top.rect.right + 10, ViewKit.Device.share().isPhoneX ? 40 + 35: 40)
      v.text = ''
      this._giftBtn = v
    }
    return this._giftBtn
  }

  get play_mid_bg () {
    if (this._play_mid_bg == null) {
      let v = null
      v = new ViewKit.View(ViewKit.Rect.rect(0, 0 , this.playBtn.width - 5, this.playBtn.height - 5))
      v.position = this.playBtn.bounds.center
      this._play_mid_bg = v
    }
    return this._play_mid_bg
  }

  get play_mid () {
    if (this._play_mid == null) {
      let v = null
      v = new ViewKit.ImageView(ViewKit.getRes('play_mid'), ViewKit.Rect.rect(0, 0 , 207 * 3, 77))
      v.alpha = 0.7
      v.imageMode = 'repeat'
      v.position = this.playBtn.bounds.center
      this._play_mid = v
    }
    return this._play_mid
  }

  get playBtn () {
    if (this._playBtn == null) {
      this._playBtn = new ViewKit.Button(ViewKit.Rect.rect(0, 0, 207, 77))
      this._playBtn.text = ''
      this._playBtn.bgImage = 'images/play_bottom.png'
      this._playBtn.cornerRadius = 50
      this._playBtn.anchorY = 0
      this._playBtn.position = new ViewKit.Point(this.width / 2, this.duckImageView.rect.bottom + 8)
    }
    return this._playBtn
  }

  get duckImageView () {
    if (this._duckImageView == null) {
      let v = null
      // v = new ViewKit.ImageView(ViewKit.getRes('home_duck'), ViewKit.Rect.rect(0, 0, 71, 83))
      v = new DuckHead(ViewKit.Rect.rect(0, 0, 71, 71))
      v.setUIDefault()
      this._duckImageView = v
      v.anchorY = 0
      v.position = new ViewKit.Point(this.width / 2, this.recordBgImageView.rect.bottom + 10)
    }
    return this._duckImageView
  }

  get recordBgImageView () {
    if (this._recordBgView == null) {
      let v = null
      v = new ViewKit.ImageView(ViewKit.getRes('pass_record'), ViewKit.Rect.rect(0, 0, 140, 52))
      v.position = new ViewKit.Point(this.width / 2, this.shareBtn.rect.bottom + 50)
      this._recordBgView = v
    }
    return this._recordBgView
  }

  get recordLabel () {
    if (this._recordLabel == null) {
      let v = null
      v = new ViewKit.Label(ViewKit.Rect.rect(0, 0, this.recordBgImageView.width, this.recordBgImageView.height - 12))
      // v.position = this.recordBgImageView.bounds.center
      v.text = '已过0关'
      v.textAlign = 'center'
      this._recordLabel = v
    }
    return this._recordLabel
  }

  get loaddingLabel() {
    if (this._loaddingLabel == null) {
        this._loaddingLabel = new ViewKit.Label(ViewKit.Rect.rect(0, 0, 150, 20))
        this._loaddingLabel.textColor = '#666666'
        this._loaddingLabel.text = '连接中'
        this._loaddingLabel.setFont(14)
        this._loaddingLabel.textAlign = 'center'
        this._loaddingLabel.anchorY = 0
        this._loaddingLabel.position = new ViewKit.Point(this.width / 2, this.playBtn.center.y)
    }

    return this._loaddingLabel

  }

  get title_1() {
    if (this._title_1 == null) {
        this._title_1 = new ViewKit.ImageView(ViewKit.getRes('title_1'), ViewKit.Rect.rect(0, 0, 100, 41.5))
        this._title_1.anchorX = 0
        this._title_1.anchorY = 0
        this._title_1.name = '1'
        this._title_1.position = new ViewKit.Point(this.width / 2 + 12, 157)
    }

    return this._title_1

  }

  get title_2() {
    if (this._title_2 == null) {
        this._title_2 = new ViewKit.ImageView(ViewKit.getRes('title_2'), ViewKit.Rect.rect(0, 0, 100, 41.5))
        this._title_2.anchorX = 1
        this._title_2.anchorY = 0
        this._title_2.name = '2'
        this._title_2.position = new ViewKit.Point(this.width / 2, 157)
    }

    return this._title_2

  }

  get duck_left() {
    if (this._duck_left == null) {
        this._duck_left = new ViewKit.ImageView(ViewKit.getRes('duck_left'), ViewKit.Rect.rect(0, 0, 49, 51.5))
        this._duck_left.anchorX = 0.5
        this._duck_left.anchorY = 1
        this._duck_left.name = '3'
        this._duck_left.position = new ViewKit.Point(this.title_2.rect.center.x, this.title_2.y - 5)
    }

    return this._duck_left

  }

  get duck_top() {
    if (this._duck_top == null) {
        this._duck_top = new ViewKit.ImageView(ViewKit.getRes('title_duck'), ViewKit.Rect.rect(0, 0, 57, 57 * 2.7))
        this._duck_top.anchorX = 0.5
        this._duck_top.anchorY = 0
        this._duck_top.name = '4'
        this._duck_top.position = new ViewKit.Point(this.title_1.center.x + 25, 0)
    }

    return this._duck_top

  }

  get rankBtn () {
    if (this._rankBtn == null) {
      let v = null
      v = new ViewKit.Button(ViewKit.Rect.rect(0, 0, 40, 56))
      v.bgImage = ViewKit.getRes('btn_rank')
      v.anchorY = 0
      v.anchorX = 0
      v.text = ''
      v.position = new ViewKit.Point(this.voiceBtn.x, this.voiceBtn.rect.bottom + 20)
      this._rankBtn = v
    }
    return this._rankBtn
  }

  get voiceBtn () {
    if (this._voiceBtn == null) {
      let v = null
      v = new ViewKit.Button(ViewKit.Rect.rect(0, 0, 40, 56))
      v.bgImage = ViewKit.getRes('btn_voice')
      v.bgImage = ViewKit.getRes(DataBus.share().openVoice ? 'btn_voice' : 'btn_voice_close')
      v.anchorY = 0
      v.anchorX = 0
      v.text = ''
      v.position = new ViewKit.Point(14, 44)
      this._voiceBtn = v
    }
    return this._voiceBtn
  }

  get shareBtn () {
    if (this._shareBtn == null) {
      let v = null
      v = new ViewKit.Button(ViewKit.Rect.rect(0, 0, 40, 56))
      v.bgImage = ViewKit.getRes('btn_wechat')
      v.anchorY = 0
      v.anchorX = 0
      v.text = ''
      v.position = new ViewKit.Point(this.voiceBtn.x, this.rankBtn.rect.bottom + 20)
      this._shareBtn = v
    }
    return this._shareBtn
  }

  get moreBtn () {
    if (this._moreBtn == null) {
      let v = null
      // v = new RandomJumpGameView()
      v = new ViewKit.Button(ViewKit.Rect.rect(0, 0, 32, 42))
      v.bgImage = ViewKit.getRes('moreGame')
      v.anchorX = 0
      v.anchorY = 0
      v.text = ''
      v.position = new ViewKit.Point(0, this.playBtn.y)
      this._moreBtn = v
    }
    return this._moreBtn
  }

  get dailyReward () {
    if (this._dailyReward == null) {
      let v = null
      v = new ViewKit.Button(ViewKit.Rect.rect(0, 0, 71.5, 75))
      v.bgImage = ViewKit.getRes('daily_reward')
      v.anchorX = 1
      v.anchorY = 0
      v.position = new ViewKit.Point(this.width - 10, this.shareBtn.rect.center.y + 10)
      v.text = ''
      // v.isHidden = true
      this._dailyReward = v
    }
    return this._dailyReward
  }


  subscribe(before, now, key, instance) {
    switch (key) {
      case 'userInfo':
      if (now) {
        this.playBtn.isHidden = false
        this.loaddingLabel.isHidden = true
        this.menus.map(x => {
          x.isHidden = false
        })
      }
      this.updateScore()
      break
      case 'gameProcessPosition':
      this.updateScore()
      break;
      default:
    }
  }

  updateScore() {
    let score = User.share().selfScore
    if (score) {
      this.recordLabel.text = '已过' + score + '关'
    }
  }



  setupUI () {
    ViewKit.SoundHelper.playSoundWithName('start')
    User.share().registerObserver(this, 'userInfo')
    User.share().registerObserver(this, 'gameProcessPosition')

    this.bgColor = 'white'

    this.menuGap = (this.width - 4 * 60) / 4
    let views = [this.bgImageView, this.playBtn, this.title_1, this.title_2, this.duck_top, this.duck_left, this.loaddingLabel,
      this.rankBtn, this.voiceBtn, this.shareBtn, this.moreBtn, this.dailyReward, this.duckImageView, this.recordBgImageView, this.giftBtn]
      views.map((x) => {
        this.addSubview(x)
      })

      let plays = [this.play_mid_bg, this.play_top]

      plays.map(x => this.playBtn.addSubview(x))

      this.play_mid_bg.maskToBounds = true
      this.play_mid_bg.cornerRadius = this.play_mid_bg.height / 2
      this.play_mid_bg.addSubview(this.play_mid)
      //animation
      ViewKit.Animation.get(this.play_mid, {loop: true})
      .to({'translation.x': this.playBtn.width - 57}, 2000)
      .call(() => {
        this.play_mid.translation.x = 0
      })
      .start()

      this.recordBgImageView.addSubview(this.recordLabel)

      this.playBtn.isHidden = User.share().userInfo == null
      this.loaddingLabel.isHidden = User.share().userInfo != null
      this.menus = [this.rankBtn, this.voiceBtn, this.shareBtn, this.recordBgImageView]
      this.menus.map(x => {
        x.isHidden = User.share().userInfo == null
      })



      this.title_1.translation.y = -this.title_1.y - this.title_1.height
      this.duck_top.translation.y = this.title_1.translation.y


      this.title_2.translation.x = -this.title_2.rect.right
      this.duck_left.translation.x = this.title_2.translation.x

      ViewKit.Animation.get(this.title_2)
      .wait(1000)
      .to({'translation.x': 0}, 500)
      .start()

      ViewKit.Animation.get(this.duck_left)
      .wait(1000)
      .to({'translation.x': 0}, 500)
      .call(() => {

        ViewKit.Animation.get(this.title_1)
        .wait(500)
        .to({'translation.y': 0}, 500)
        .start()

        ViewKit.Animation.get(this.duck_top)
        .wait(500)
        .to({'translation.y': 0}, 500)
        .start()
      })
      .start()

      let timeGap = 1000
      this.giftBtn.animationAnchorY = 0
      ViewKit.Animation.get(this.giftBtn, {loop: true})
      .wait(timeGap)
      .to({'rotate': Math.PI / 10}, timeGap)
      .to({'rotate': -Math.PI / 10}, timeGap * 2)
      .to({'rotate': 0}, timeGap)
      .start()

      this.awardBtnAnimation()
    }

    awardBtnAnimation() {

      let timeInterval = 150
      let offset = 0.08
      let translationY = 15

      this.dailyReward.animationAnchorY = 1
      ViewKit.Animation.get(this.dailyReward, {loop: true})
      .to({'scale.x': 1 + offset, 'scale.y': 1 - offset}, timeInterval)
      .to({'scale.x': 1 - offset, 'scale.y': 1 + offset, 'translation.y': -translationY}, timeInterval * 2)
      .to({'scale.x': 1, 'scale.y': 1, 'translation.y': 0}, timeInterval)
      .to({'scale.x': 1 + offset, 'scale.y': 1 - offset}, timeInterval / 2)
      .to({'scale.x': 1, 'scale.y': 1}, timeInterval / 2)
      .wait(2000)
      .start()

    }

    addActionTarget() {

      this.shareBtn.clickedBlock = () => {
        ShareHelper.share()
      }

      // this.moreBtn.clickedBlock = () => {
      //
      //   wx.navigateToMiniProgram({
      //     appId: 'wxf945a1bc6393a957'
      //   })
      // }

      this.voiceBtn.clickedBlock = () => {

        wx.aldSendEvent("音效")
        DataBus.share().openVoice = !DataBus.share().openVoice
        this.voiceBtn.bgImage = ViewKit.getRes(DataBus.share().openVoice ? 'btn_voice' : 'btn_voice_close')
      }

    }
  }

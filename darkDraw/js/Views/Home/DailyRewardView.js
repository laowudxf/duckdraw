import * as ViewKit from '../../libs/ViewKit/index'
import ADHelper from '../../Helper/ADHelper'
import User from '../../DataModel/User'
import DataBus from '../../DataBus'

export default class DailyRewardView extends ViewKit.View{

  get coinBgView () {
    if (this._coinBgView == null) {
      let v = null
      v = new ViewKit.View(ViewKit.Rect.rect(0, 0, 300, 240))
      v.bgColor = 'white'
      v.cornerRadius = 15
      v.position = new ViewKit.Point(this.width / 2, ViewKit.Device.share().screenHeight < 600 ? (this.height / 2 - 100): (this.height / 2) )
      this._coinBgView = v
    }
    return this._coinBgView
  }

  get coinImageView () {
    if (this._coinImageView == null) {
      let v = null
      v = new ViewKit.ImageView(ViewKit.getRes('coin_100'), ViewKit.Rect.rect(0, 0, 315, 200))
      v.position = this.coinBgView.bounds.center
      this._coinImageView = v
    }
    return this._coinImageView
  }

  get title () {
    if (this._title == null) {
      let v = null
      v = new ViewKit.Label()
      v.position = new ViewKit.Point(this.coinBgView.width / 2, 30)
      v.text = '每日登陆奖励'
      v.textAlign = 'center'
      v.textColor = '#666666'
      this._title = v
    }
    return this._title
  }

  get closeBtn () {
    if (this._closeBtn == null) {
      let v = null
      v = new ViewKit.Button(ViewKit.Rect.rect(0, 0, 36, 36))
      v.bgImage = ViewKit.getRes('close_1')
      v.text = ''
      v.position = new ViewKit.Point(this.coinBgView.rect.right, this.coinBgView.y)
      this._closeBtn = v
    }
    return this._closeBtn
  }

  get descLabel () {
    if (this._descLabel == null) {
      let v = null
      v = new ViewKit.Label()
      v.text = '恭喜你，获得100金币奖励！'
      v.textAlign = 'center'
      v.setFont(15)
      v.textColor = '#666666'
      v.position = new ViewKit.Point(this.coinBgView.width / 2, this.coinBgView.bounds.bottom - 20)
      this._descLabel = v
    }
    return this._descLabel
  }

  get getBtn () {
    if (this._getBtn == null) {
      let v = null
      v = new ViewKit.Button(ViewKit.Rect.rect(0, 0, 175, 60))
      v.bgImage = ViewKit.getRes('free_get_video')
      v.anchorY = 0
      v.text = ''
      v.position = new ViewKit.Point(this.width / 2, this.coinBgView.rect.bottom + 40)
      this._getBtn = v
    }
    return this._getBtn
  }

  setupUI() {
    this.bgColor = 'rgba(0, 0, 0, 0.7)'
    let views = [this.coinBgView, this.getBtn, this.closeBtn]
    this.isGamePage = false

    views.map(x => this.addSubview(x))

    views = [this.title, this.coinImageView, this.descLabel]

    views.map(x => this.coinBgView.addSubview(x))
  }


  willMoveToSuperview(view) {
    if (view == null) {
        if (this.closeBlock) {
          this.closeBlock()
        }
    }
  }
  addActionTarget() {
    this.closeBtn.clickedBlock = () => {

      wx.aldSendEvent("每日登陆页-关闭")
      this.removeFromParent()
    }

    this.getBtn.clickedBlock = () => {
      wx.aldSendEvent("每日登陆页-免费领奖")
      let video = ADHelper.createVideoAd(ADHelper.adConfig[this.isGamePage ? 'video_daily_game':'video_daily'], () => {

        User.share().coin += 100

        wx.showModal({
          title: '提示',
          content: '+100金币',
          showCancel: false
        })

        DataBus.share().lastGetDailyRewardTime = Date.now()

        this.removeFromParent()

      }, () => {

      })
      ADHelper.showVideoAd(video)
    }
  }



}

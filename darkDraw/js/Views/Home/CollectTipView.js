
import * as ViewKit from '../../libs/ViewKit/index'
import ADHelper from '../../Helper/ADHelper'

export default class CollectTipView extends ViewKit.View {
  get tipImageView () {
    if (this._tipImageView == null) {
      let v = null
      v = new ViewKit.ImageView(ViewKit.getRes('collect_tip'), ViewKit.Rect.rect(0, 0, 265, 252))
      v.position = this.bounds.center
      this._tipImageView = v
    }
    return this._tipImageView
  }

  get closeBtn () {
    if (this._closeBtn == null) {
      let v = null
      v = new ViewKit.Button(ViewKit.Rect.rect(0, 0, 34, 34))
      v.bgImage = ViewKit.getRes('close_red')
      v.position = new ViewKit.Point(this.tipImageView.rect.right, this.tipImageView.y)
      v.text = ''
      this._closeBtn = v
    }
    return this._closeBtn
  }

  setupUI() {
    this.bgColor = 'rgba(0, 0, 0, 0.7)'
    this.bannerAd = ADHelper.createBannerAD(ADHelper.adConfig['banner_home'])

    // this.bannerAd.show()
    // .then(() => {
    //   if (this.bannerAd) {
    //     // bannerAd.style.top = ViewKit.Device.share().screenHeight - bannerAd.style.realHeight
    //     this.bannerAd.style.width = 100
    //     // this.bannerAd.style.top = (this.height + 345) / 2 - bannerAd.style.realHeight
    //     this.bannerAd.style.top = ((this.height + 345) / 2) - (this.bannerAd.style.realHeight)
    //   }
    //
    // })
    // this.bgColor = 'black'
    let views = [this.tipImageView, this.closeBtn]

    views.map((x) => {
      this.addSubview(x)
    })
  }

  willMoveToSuperview(view) {
    if (view == null) {
      this.bannerAd.hide()
      this.bannerAd.destory()
    }
  }

}

import * as ViewKit from '../../libs/ViewKit/index'

export default class RandomJumpGameView extends ViewKit.View {

  get gameImageView () {
    if (this._gameImageView == null) {
      let v = null
      v = new ViewKit.Button(ViewKit.Rect.rect(5, 5, 47, 47))
      v.text = ''
      // v.bgImageView.cornerRadius = 5
      this._gameImageView = v
    }
    return this._gameImageView
  }

  get gameName () {
    if (this._gameName == null) {
      let v = null
      v = new ViewKit.Label()
      v.textColor = 'white'
      v.textAlign = 'center'
      v.height = 20
      v.anchorY = 0
      v.setFont(12)
      v.position = new ViewKit.Point(this.width / 2, this.gameImageView.rect.bottom)
      this._gameName = v
    }
    return this._gameName
  }

  setupUI() {
    this.bgColor = '#4c5052'
    this.cornerRadius = 5
    this.width = this.gameImageView.width + 10
    this.height = this.gameImageView.height + 14 + 10
    let views = [this.gameImageView, this.gameName]

    views.map(x => this.addSubview(x))

    this.randomGame()

    this.gameImageView.clickedBlock = () => {
        wx.aldSendEvent("轮播图小程序")
      wx.navigateToMiniProgram({appId: this.appid})
    }

    setInterval(() => {
      this.randomGame()
    }, 10000)
  }

  randomGame() {
			let map = [
				['wx32f39e398bd85f6d',"脑力挑战"],
				['wx17c68d004a3e2685',"欢乐连连"],
				['wxf945a1bc6393a957',"六角消除"],
				// ['wxbd66bb89ec2b9805',"来赚一亿"],
				['wx98edb9fd36968d0a',"成语接龙"],
				['wx03eb4405d98fd065',"每天步赚"],
				['wx0154c51079b81af8',"皇上吉祥"],
				['wxbe04dd8bfd5ad65c',"王牌狙击"],
				['wx5857c62152010a85',"拆散情侣"],
				['wx36b6c21b34074aa6',"头条视频"],
				['wx8113af51e011f04c',"猜图大全"],
			]

			let index = Math.floor(Math.random() * 10)
			let info = map[index]

      this.appid = info[0]
      this.gameName.text = info[1]
      this.gameImageView.bgImage = ViewKit.getRes('moreGame/0' + (index + 1))
  }
}

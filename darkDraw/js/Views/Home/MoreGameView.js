
import * as ViewKit from '../../libs/ViewKit/index'

export default class MoreGameView extends ViewKit.View {

  get bgView () {
    if (this._bgView == null) {
      let v = null
      v = new ViewKit.View(ViewKit.Rect.rect(0, 0, 220, 320))
      v.cornerRadiusArr = [0, 10, 10, 0]
      v.borderWidth = 2
      v.borderColor = '#87c9e8'
      v.bgColor = '#dceef5'
      v.anchorX = 0
      v.position = new ViewKit.Point(0, this.height / 2)
      this._bgView = v
    }
    return this._bgView
  }

  get closeBtn () {
    if (this._closeBtn == null) {
      let v = null
      v = new ViewKit.Button(ViewKit.Rect.rect(0, 0, 34, 34))
      v.bgImage = ViewKit.getRes('close_blue')
      v.position = new ViewKit.Point(this.bgView.rect.right, this.bgView.y)
      v.text = ''
      this._closeBtn = v
    }
    return this._closeBtn
  }


  setupUI() {
    this.bgColor = 'rgba(0, 0, 0, 0.4)'

    let views = [this.bgView, this.closeBtn]

    views.map((x) => {
      this.addSubview(x)
    })

    this.setupOtherGame()

  }

  setupOtherGame() {

    let map = [
      ['wx32f39e398bd85f6d',"脑力挑战"],
      ['wx17c68d004a3e2685',"欢乐连连"],
      ['wxf945a1bc6393a957',"六角消除"],
      // ['wxbd66bb89ec2b9805',"来赚一亿"],
      ['wx98edb9fd36968d0a',"成语接龙"],
      ['wx03eb4405d98fd065',"每天步赚"],
      ['wx0154c51079b81af8',"皇上吉祥"],
      ['wxbe04dd8bfd5ad65c',"王牌狙击"],
      ['wx5857c62152010a85',"拆散情侣"],
      ['wx36b6c21b34074aa6',"头条视频"],
      ['wx8113af51e011f04c',"猜图大全"],
    ]

    map.map((x, index) => {
      let v = new ViewKit.Button(ViewKit.Rect.rect(0, 0, 44, 44))
      v.bgImage = ViewKit.getRes('moreGame/0' + (index + 1))
      v.cornerRadius = v.height / 2
      v.maskToBounds = true
      v.text = ''
      let a = index % 3
      let b = Math.floor(index / 3)
      v.x = a * (v.width + 20)  + 20
      v.y = b * (v.height + 30) + 20
      v.clickedBlock = () => {
        wx.navigateToMiniProgram({appId: x[0]})
      }

      let label = new ViewKit.Label()
      label.text = x[1]
      label.textAlign = 'center'
      label.setFont(10)
      label.textColor = '#666666'
      label.position = new ViewKit.Point(v.rect.center.x, v.rect.bottom + 10)
      this.bgView.addSubview(label)
      this.bgView.addSubview(v)
    })
  }

}

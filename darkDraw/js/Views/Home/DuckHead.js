import * as ViewKit from '../../libs/ViewKit/index'

export default class DuckHead extends ViewKit.View {

  get tail () {
    if (this._tail == null) {
      let v = null
      v = new ViewKit.View(ViewKit.Rect.rect(0, 0, 18, 18))
      v.anchorX = 0.5
      v.anchorY = 0.5
      v.bgColor = '#ffe33f'
      v.cornerRadius = 9
      v.position = new ViewKit.Point(this.width / 2, 0)
      this._tail = v
    }
    return this._tail
  }

  get body () {
    if (this._body == null) {
      let v = null
      v = new ViewKit.View(ViewKit.Rect.rect(0, 0, this.width * 0.8, this.width * 0.4))
      v.cornerRadius = 5
      v.bgColor = '#d5a728'
      v.anchorX = 0.5
      v.anchorY = 0.5
      v.position = new ViewKit.Point(this.width / 2, this.head.y)
      this._body = v
    }
    return this._body
  }

  get hair () {
    if (this._hair == null) {
      let v = null
      v = new ViewKit.ImageView(null , ViewKit.Rect.rect(0, 0, this.width * 0.14, this.width * 0.17))
      v.anchorX = 0.5
      v.anchorY = 0.5
      v.position = new ViewKit.Point(this.head.width / 2, 0)
      this._hair = v
    }
    return this._hair
  }

  get head () {
    if (this._head == null) {
      let v = null
      v = new ViewKit.ImageView(null , ViewKit.Rect.rect(0, 0, this.width * 0.9, this.width * 0.4))
      v.anchorX = 0.5
      v.anchorY = 1
      v.position = new ViewKit.Point(this.width / 2, this.height - this.width * 0.05)
      this._head = v
    }
    return this._head
  }

  get footLeft () {
    if (this._footLeft == null) {
      let v = null
      v = new ViewKit.ImageView(ViewKit.getRes('duckImage/foot'), ViewKit.Rect.rect(0, 0, this.width * 0.12, this.width * 0.15))
      v.anchorX = 0.5
      v.anchorY = 0
      v.position = new ViewKit.Point(this.width * (0.5 - 0.2), this.head.rect.bottom)
      this._footLeft = v
    }
    return this._footLeft
  }

  get footRight () {
    if (this._footRight == null) {
      let v = null
      v = new ViewKit.ImageView(ViewKit.getRes('duckImage/foot'), ViewKit.Rect.rect(0, 0, this.width * 0.12, this.width * 0.15))
      v.anchorX = 0.5
      v.anchorY = 0
      v.position = new ViewKit.Point(this.width * (0.5 + 0.2), this.head.rect.bottom)
      this._footRight = v
    }
    return this._footRight
  }

  get mouse () {
    if (this._mouse == null) {
      let v = null
      v = new ViewKit.ImageView(null , ViewKit.Rect.rect(0, 0, this.width * 0.41, this.width * 0.2))
      v.anchorX = 0.5
      v.anchorY = 1
      v.position = new ViewKit.Point(this.width / 2, this.head.rect.bottom - 2)
      this._mouse = v
    }
    return this._mouse
  }

  get eyeLeft () {
    if (this._eyeLeft == null) {
      let v = null
      v = new ViewKit.ImageView(ViewKit.getRes('duckImage/eye_open'), ViewKit.Rect.rect(0, 0, this.width * 0.14, this.width * 0.14))
      v.anchorX = 0.5
      v.anchorY = 0.5
      v.position = new ViewKit.Point(this.width / 2 - this.width * 0.25, this.head.rect.center.y)
      this._eyeLeft = v
    }
    return this._eyeLeft
  }

  get eyeRight () {
    if (this._eyeRight == null) {
      let v = null
      v = new ViewKit.ImageView(ViewKit.getRes('duckImage/eye_open'), ViewKit.Rect.rect(0, 0, this.width * 0.14, this.width * 0.14))
      v.anchorX = 0.5
      v.anchorY = 0.5
      v.position = new ViewKit.Point(this.width / 2 + this.width * 0.25, this.head.rect.center.y)
      this._eyeRight = v
    }
    return this._eyeRight
  }

  // get footer () {
  //   if (this._footer == null) {
  //     let v = null
  //     v = new ViewKit.ImageView(null, ViewKit.Rect.rect(0, 0, ))
  //     this._footer = v
  //   }
  //   return this._footer
  // }

   setupUI() {
     this.autoAnimation = true
     this.bgColor = '#fece00'
     this.cornerRadius = 10
     let views = [this.tail, this.body, this.head, this.mouse, this.eyeLeft, this.eyeRight, this.footLeft, this.footRight]
     views.map(x => this.addSubview(x))
     this.head.addSubview(this.hair)
   }

   asGameHead() {
     this.tail.isHidden = true
   }

   updateModel(model) {
     this.head.imageSrc = model.img1
     this.hair.imageSrc = model.img2
     this.mouse.imageSrc = model.img3
     if (model.rgb.length == 4) {
       this.tail.bgColor = model.rgb[0]
       this.body.bgColor = model.rgb[1]
       this.bgColor = model.rgb[3]
     }
   }

   setUIDefault() {
     this.hair.imageSrc = ViewKit.getRes('duckImage/hair')
     this.head.imageSrc = ViewKit.getRes('duckImage/head')
     this.mouse.imageSrc = ViewKit.getRes('duckImage/mouse')

   }

   willMoveToSuperview(view) {
     if (view == null) {
       return
     }
     if (this.autoAnimation) {
       this.animation()
     }
   }

   animation() {
     this.tail.animationAnchorX = 0.5
     this.tail.animationAnchorY = 1

     let time = 150
     let offset = 0.2

      ViewKit.Animation.get(this.tail, {loop: true})
      .wait(time * 2)
      .to({'scale.y': 1 - offset, 'scale.x': 1 + offset}, time)
      .to({'scale.y': 1 + offset, 'scale.x': 1 - offset}, time * 2)
      .to({'scale.y': 1, 'scale.x': 1}, time)
      .wait(time * 2)
      .start()

     this.head.animationAnchorX = 0.5
     this.head.animationAnchorY = 1
     let headTime = 200

     this.eyeLeft.animationAnchorX = 0.5
     this.eyeLeft.animationAnchorY = 0
     let flag_left = 1

      this.eyeRight.animationAnchorX = 0.5
      this.eyeRight.animationAnchorY = 0
      let flag_right = 1

     ViewKit.Animation.get(this.head, {loop: true})
     .call(() => {
        ViewKit.Animation.get(this.eyeLeft)
        .to({'scale.y': 0}, headTime)
        .call(() => {
          this.eyeLeft.imageSrc = ViewKit.getRes('duckImage/' + (flag_left ? 'eye_close_left' : 'eye_open'))
          flag_left = flag_left ? 0 : 1
        })
        .to({'scale.y': 1}, headTime)
        .wait(headTime * 2)
        .start()

      ViewKit.Animation.get(this.eyeRight)
      .to({'scale.y': 0}, headTime)
      .call(() => {
        this.eyeRight.imageSrc = ViewKit.getRes('duckImage/' + (flag_right ? 'eye_close_right' : 'eye_open'))
        flag_right = flag_right ? 0 : 1
      })
      .to({'scale.y': 1}, headTime)
      .wait(headTime * 2)
      .start()

      })
      .to({'scale.y': 1 + offset}, headTime)
      .to({'scale.y': 1}, headTime)
      .wait(headTime * 2)
      .start()
    }

    stopAnimation() {
      let views = [this.tail, this.eyeLeft, this.eyeRight, this.body]

      views.map(x => {
        x.removeAnimation()
      })
    }
  }

import * as ViewKit from '../../libs/ViewKit/index'

export default class DuckTail extends ViewKit.View {

  get tail () {
    if (this._tail == null) {
      let v = null
      v = new ViewKit.View(ViewKit.Rect.rect(0, 0, 18, 18))
      v.anchorX = 0.5
      v.anchorY = 0.5
      v.bgColor = '#ffe33f'
      v.cornerRadius = 9
      v.position = new ViewKit.Point(this.width / 2, 0)
      this._tail = v
    }
    return this._tail
  }
  setupUI() {
    this.addSubview(this.tail)

     this.tail.animationAnchorX = 0.5
     this.tail.animationAnchorY = 1

     let time = 150
     let offset = 0.2

      ViewKit.Animation.get(this.tail, {loop: true})
      .wait(time * 2)
      .to({'scale.y': 1 - offset, 'scale.x': 1 + offset}, time)
      .to({'scale.y': 1 + offset, 'scale.x': 1 - offset}, time * 2)
      .to({'scale.y': 1, 'scale.x': 1}, time)
      .wait(time * 2)
      .start()
  }
}

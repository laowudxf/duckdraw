import * as ViewKit from '../../libs/ViewKit/index'
import DataBus from '../..//DataBus'
import ShareHelper from '../../Helper/ShareHelper'

export default class RankBoard extends ViewKit.View {

  get groupBtn () {
    if (this._groupBtn == null) {
      let v = null
      v = new ViewKit.Button(ViewKit.Rect.rect(0, 0, 183, 50))
      v.anchorY = 1
      v.cornerRadius = 25
      v.bgColor = '#faab34'
      v.position = new ViewKit.Point(this.width / 2, this.closeBtn.y - 25)
      v.label.text = '查看群排行'
      v.label.textColor = 'white'
      this._groupBtn = v
    }
    return this._groupBtn
  }

  get closeBtn () {
    if (this._closeBtn == null) {
      let v = null
      v = new ViewKit.Button(ViewKit.Rect.rect(0, 0, 25, 25))
      v.bgImage = ViewKit.getRes('close_dark')
      v.label.text = ''
      this._closeBtn = v
      v.anchorY = 1
      v.position = new ViewKit.Point(this.width / 2, this.height - 25)
    }
    return this._closeBtn
  }
  setupUI() {
    this.bgColor = 'white'
    let views = [this.closeBtn, this.groupBtn]
    views.map(x => {
      this.addSubview(x)
    })
    console.log(this.height - this.groupBtn.y);
  }

  willMoveToSuperview(view) {
    if (view == null) {
      DataBus.share().hiddenBoard()
    } else {
      DataBus.postMessage({
        method: 'refreshBoard',
        param: {}
      })
      DataBus.share().showRankBoard()
    }
  }

  addActionTarget() {
    this.groupBtn.clickedBlock = () => {
        wx.aldSendEvent("排行页-查看群排行")
      let shareTicket = DataBus.share().shareTicket
      if (shareTicket == null) {
        ShareHelper.share()
        .then(() => {
          DataBus.postMessage({
            method: 'refreshBoard',
            param: {
              ticket: shareTicket
            }
          })
        })
        return
      }
      DataBus.postMessage({
        method: 'refreshBoard',
        param: {
          ticket: shareTicket
        }
      })
    }
  }

}

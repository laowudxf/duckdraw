import * as ViewKit from '../../libs/ViewKit/index'
import CheckPointView from '../CheckPointView'
import GameDrawView from '../Draw/GameDrawView'
import GameBoardView from '../Draw/GameBoardView'
import DataBus from '../../DataBus'
import PassPathView from './PassPathView'
import User from '../../DataModel/User'
import RandomMoreGameView_checkView from './RandomMoreGameView_checkView'

export default class PassDuckItemPass extends ViewKit.View {

  get moreGame () {
    if (this._moreGame == null) {
      let v = null
      v = new RandomMoreGameView_checkView(ViewKit.Rect.rect(0, 0, this.width - 40, 95))
      v.anchorY = 1
      v.position = new ViewKit.Point(this.width / 2, this.height)
      this._moreGame = v
    }
    return this._moreGame
  }

  get backBtn() {
    if(this._backBtn == null) {
      let y = 17
      if (ViewKit.Device.share().isPhoneX) {
         y += 10
      }
      this._backBtn = new ViewKit.Button(ViewKit.Rect.rect(0, y, 72, 41.5))
      this._backBtn.text = ''

      this._backBtn.bgImage = ViewKit.getRes('btn_back')
    }
    return this._backBtn
  }


  get topDuck() {
    if(this._topDuck == null) {
      this._topDuck = new ViewKit.ImageView(null, ViewKit.Rect.rect(0, 0, 98, 93))
      this._topDuck.anchorY = 0
      this._topDuck.position = new ViewKit.Point(this.width / 2, 0)
    }
    return this._topDuck
  }


  get passScrollView() {
    if (this._passScrollView == null) {
      // this._passScrollView = new ViewKit.ScrollView(ViewKit.Rect.rect(0, 0, this.width - 70, this.height - 15 - this.topDuck.rect.bottom - 69))
      this._passScrollView = new ViewKit.ScrollView(ViewKit.Rect.rect(0, 0, this.width - 70, this.height - 15 - this.topDuck.rect.bottom - this.moreGame.height))
      this._passScrollView.anchorY = 0
      this._passScrollView.position = new ViewKit.Point(this.width / 2, this.topDuck.rect.bottom + 15)
      this._passScrollView.bgColor = 'white'
    }
    return this._passScrollView
  }

  setupUI() {
    this.passItemViews = []
    this.colume = 3
    this.itemGap = 15
    this.itemWidth = (this.passScrollView.width - ((this.colume - 1) * this.itemGap)) / this.colume
    this.itemHeight = this.itemWidth

    this.bgColor = 'white'
    let views = [this.backBtn, this.topDuck, this.passScrollView, this.moreGame] // , this.passScrollView]
    views.map(x => {
      this.addSubview(x)
    })
  }

  addActionTarget() {
    this.backBtn.clickedBlock = () => {
      let v = new CheckPointView(this.bounds)
      ViewKit.App.share().rootView = v
    }
  }

  get dataModel() {
    return this._dataModel
  }

  set dataModel(value) {
    this._dataModel = value
    // wx.showLoading()
    // setTimeout(() => {
    this.update(value)
    // }, 50)
  }

  update(model) {
    console.log(model);
    let last = null
    this.topDuck.imageSrc = model.pic2
    let randomPaths = []
    let drawView = new GameBoardView(this.bounds)
    drawView.justCalcPath = true
    this.passItemViews.map(x => x.removeFromParent())
    model.pass.map((x, index) => {
      let row = parseInt(index / this.colume)
      let colume = (index % this.colume)
      let v = null
      let state = DataBus.share().isPassGroupPassLock(model.id, index)
      let rect = ViewKit.Rect.rect(colume * (this.itemGap + this.itemWidth), row * (this.itemHeight + this.itemGap), this.itemWidth, this.itemHeight)
      if (state == 1) {
        v = new PassPathView(rect)
        let passPath = DataBus.share().passPathWith(model.id, index)
        v.themeColor = model.desc
        if (passPath) {
          v.passPathData = passPath
        } else {
          let passData = JSON.parse(x.content)
          drawView.dataModel = passData
          let passPath = drawView.randomValidPassPath()
          randomPaths.push([this.dataModel.id, index, passPath])
          v.passPathData = passPath
          drawView.tipPaths = null
        }

      } else {
        v = new ViewKit.ImageView(null, rect)
        v.userInteraction = true
        v.state = state
        v.imageSrc = ViewKit.getRes(v.state == -1 ? 'pass_duck_pass_lock' : 'pass_duck_pass_valid')
        v.bgColor = 'green'
      }

      let tap = new ViewKit.TapGestureRecognzer(v, () => {
        if (v.state == -1) {
          return
        }
        ViewKit.SoundHelper.playSoundWithName('button')

        if (this.itemClick) {
          this.itemClick(this.dataModel, index)
        }
      })
      v.addGesture(tap)
      this.passScrollView.contentView.addSubview(v)
      this.passItemViews.push(v)
      last = v
    })


    if (randomPaths.length > 0) {
      let userPassPaths = User.share().passPaths
      randomPaths.map(x => {
        userPassPaths[x[0] + '_' + x[1]] = x[2]
      })
      User.share().passPaths = userPassPaths
    }

    // wx.hideLoading()

    if (last) {
      this.passScrollView.contentView.height = last.rect.bottom
    }
  }

}

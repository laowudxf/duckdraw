import * as ViewKit from '../../libs/ViewKit/index'
import DataBus from '../../DataBus'
import DuckHead from '../Home/DuckHead'
import ADHelper from '../../Helper/ADHelper'
import RandomMoreGameView from './RandomMoreGameView'
import Fireworks from '../Particle/Fireworks'

export default class PassCompleteView extends ViewKit.View {
  static bannerArr = []
  static adIndex = 0
  static showdAd = null

  static randomAd() {

    for (let i = 0; i < 5; i++) {
      let ad = ADHelper.createBannerAD(ADHelper.adConfig['banner_game'])
      ad.hide()
      this.bannerArr.push(ad)
    }
  }

  static showAd() {
    let ad = this.bannerArr[this.adIndex]
    ad.show()
    this.showdAd = ad
    this.adIndex += 1
    if (this.adIndex >= this.bannerArr.length) {
      this.adIndex = 0
    }
  }

  static hideAd() {
    if (this.showdAd) {
      this.showdAd.hide()
    }

  }

  willMoveToSuperview(view) {
    if(view == null) {
      DataBus.share().showChaseView()
    // PassCompleteView.hideAd()
      return
    }
    // PassCompleteView.showAd()
    // PassCompleteView.banner.show()
    DataBus.share().showOpenDataView(false)
  }

  get duckImageView () {
    if (this._duckImageView == null) {
      let v = null
      v = new DuckHead(ViewKit.Rect.rect(0, 0, 100, 100))
      v.anchorY = 0
      v.position = new ViewKit.Point(this.topView.width / 2, 50)
      this._duckImageView = v
    }
    return this._duckImageView
  }

  get topView () {
    if (this._topView == null) {
      let v = null
      v = new ViewKit.View(ViewKit.Rect.rect(0, 0, 225, 225))
      v.anchorY = 1
      v.position = new ViewKit.Point(this.width / 2, this.height / 2)
      v.bgColor = '#9486de'
      v.cornerRadius = 10
      this._topView = v
    }
    return this._topView
  }

  get getCoinBtn () {
    if (this._getCoinBtn == null) {
      let v = null
      v = new ViewKit.Button(ViewKit.Rect.rect(0, 0, 165, 45))
      v.text = ''
      v.bgImage = ViewKit.getRes('double_get')
      v.anchorY = 1
      v.position = new ViewKit.Point(this.width / 2, this.nextPassBtn.y)
      v.isHidden = DataBus.share().preProduct
      this._getCoinBtn = v
    }
    return this._getCoinBtn
  }


  get nextPassBtn () {
    if (this._nextPassBtn == null) {
      let v = null
      v = new ViewKit.Button(ViewKit.Rect.rect(0, 0, 183, 56))
      v.text = '下一关>>'
      v.label.setFont(15)
      v.label.textColor = '#adadad'
      v.anchorY = 1
      v.position = new ViewKit.Point(this.width / 2, this.height - ViewKit.Device.share().adMaxHeight + 15)
      v.isHidden = true
      this._nextPassBtn = v
    }
    return this._nextPassBtn
  }

  get rankBoard () {
    if (this._rankBoard == null) {
      let v = null
      v = new ViewKit.Button(ViewKit.Rect.rect(0, 0, 183, 20))
      v.text = '查看全部排行榜>'
      v.label.setFont(14)
      v.label.textColor = '#adadad'
      v.anchorY = 1
      v.position = new ViewKit.Point(this.bgView.width / 2, this.bgView.height - 5)
      v.isHidden = true
      this._rankBoard = v
    }
    return this._rankBoard
  }

  get moreGame () {
    if (this._moreGame == null) {
      let v = null
      v = new RandomMoreGameView(ViewKit.Rect.rect(0, 0, 275, 60))
      v.anchorY = 0
      v.position = new ViewKit.Point(this.width / 2, this.youLikeLabel.rect.bottom + 10)
      this._moreGame = v
    }
    return this._moreGame
  }

  get coinImageView () {
    if (this._coinImageView == null) {
      let v = null
      v = new ViewKit.ImageView(ViewKit.getRes('coin'), ViewKit.Rect.rect(0, 0, 22, 22))
      v.anchorX = 1
      v.position = new ViewKit.Point(this.awardTitle.x - 3, this.awardTitle.center.y)
      this._coinImageView = v
    }
    return this._coinImageView
  }
  get awardTitle () {
    if (this._awardTitle == null) {
      let v = null
      v = new ViewKit.Label(ViewKit.Rect.rect(0, 0, 100, 20))
      v.position = new ViewKit.Point(this.coinBgView.width / 2, this.coinBgView.height / 2)
      v.text = '恭喜获得10金币~'
      v.textColor = '#666666'
      v.setFont(15)
      this._awardTitle = v
    }
    return this._awardTitle
  }

  get bgView () {
    if (this._bgView == null) {
      let v = null
      v = new ViewKit.View(ViewKit.Rect.rect(0, 0, 301, 238))
      v.anchorY = 0
      v.position = new ViewKit.Point(this.width / 2, ViewKit.Device.share().isPhoneX ? 140 : 100)
      v.cornerRadius = 8
      v.bgColor = 'white'
      this._bgView = v
    }
    return this._bgView
  }

  get passTitle () {
    if (this._passTitle == null) {
      let v = null
      v = new ViewKit.ImageView(ViewKit.getRes('pass_title_new'), ViewKit.Rect.rect(0, 0, 238, 109))
      v.position = new ViewKit.Point(this.width / 2, this.bgView.y - 25)
      this._passTitle = v
    }
    return this._passTitle
  }

  get coinBgView () {
    if (this._coinBgView == null) {
      let v = null
      v = new ViewKit.View(ViewKit.Rect.rect(0, 0, 250, 50))
      v.anchorY = 1
      v.cornerRadius = 5
      v.bgColor = '#f2f2f2'
      v.position = new ViewKit.Point(this.bgView.width / 2, this.bgView.height - 30)
      this._coinBgView = v
    }
    return this._coinBgView
  }


  get youLikeLabel () {
    if (this._youLikeLabel == null) {
      let v = null
      v = new ViewKit.Label(ViewKit.Rect.rect(0, 0, 0, 20))
      v.anchorY = 0
      v.position = new ViewKit.Point(this.width / 2, this.bgView.rect.bottom + 10)
      v.textAlign = 'center'
      v.text = '--猜你喜欢--'
      v.setFont(14)
      v.textColor = '#666666'
      this._youLikeLabel = v
    }
    return this._youLikeLabel
  }

  setupUI() {
    this.banner = ADHelper.createBannerAD(ADHelper.adConfig['banner_game'])
    this.bgColor = 'rgba(0, 0, 0, 0.7)'
    // let views = [this.topView, this.getCoinBtn, this.nextPassBtn]
    let views = [this.bgView, this.passTitle, this.nextPassBtn, this.getCoinBtn, this.youLikeLabel, this.moreGame]
    views.map(x => {
      this.addSubview(x)
    })
    this.bgView.addSubview(this.coinBgView)
    this.bgView.addSubview(this.rankBoard)

    this.coinBgView.addSubview(this.coinImageView)
    this.coinBgView.addSubview(this.awardTitle)

    views = [this.duckImageView, this.awardTitle, this.coinImageView]

    views.map(x => {
      this.topView.addSubview(x)
    })

    if (ViewKit.Device.share().screenHeight < 620) {
      console.log(11111111111);
      this.youLikeLabel.isHidden = true
      this.moreGame.isHidden = true
    }

    setTimeout(() => {
      this.nextPassBtn.isHidden = false
    }, 1000)

    // this.setupFirework()
  }

  setupFirework() {
    let v = new Fireworks(ViewKit.Rect.rect(0, 0, 10, 10))
    v.bgColor = 'red'
    v.position = this.passTitle.center
    this.addSubview(v)
  }

  addActionTarget() {
    this.nextPassBtn.clickedBlock = () => {
      if (this.nextBlock) {
        this.banner.hide()
        this.banner.destory()
        this.removeFromParent()
        this.nextBlock()
      }
    }

    this.getCoinBtn.clickedBlock = () => {
      if (this.getCoinBlock) {
        this.getCoinBlock()
      }
    }
  }

  get dataModel () {
    return this._dataModel
  }

  set dataModel(value) {
    this._dataModel = value
    this.update(value)
  }

  update(model) {
    if (model.rgb.length == 4) {
      this.topView.bgColor = model.rgb[2]
    } else {
      this.topView.bgColor = model.desc
    }
    this.duckImageView.updateModel(model)
  }
}

// PassCompleteView.randomAd()

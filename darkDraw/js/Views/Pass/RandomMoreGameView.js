
import * as ViewKit from '../../libs/ViewKit/index'

export default class RandomMoreGameView extends ViewKit.View {
  setupUI() {
    let map = [
      ['wx32f39e398bd85f6d',"脑力挑战"],
      ['wx17c68d004a3e2685',"欢乐连连"],
      ['wxf945a1bc6393a957',"六角消除"],
      // ['wxbd66bb89ec2b9805',"来赚一亿"],
      ['wx98edb9fd36968d0a',"成语接龙"],
      ['wx03eb4405d98fd065',"每天步赚"],
      ['wx0154c51079b81af8',"皇上吉祥"],
      ['wxbe04dd8bfd5ad65c',"王牌狙击"],
      ['wx5857c62152010a85',"拆散情侣"],
      ['wx36b6c21b34074aa6',"头条视频"],
      ['wx8113af51e011f04c',"猜图大全"],
    ]

    let arr = this.getRandomIndex()

    arr.map((index, i) => {
      let x = map[index]

      // let v = new ViewKit.Button(ViewKit.Rect.rect(0, 0, 64, 64))
      let v = this.createSingleView(x, index)
      let tap = new ViewKit.TapGestureRecognzer(v, () => {
        wx.navigateToMiniProgram({appId: x[0]})
      })
      v.addGesture(tap)
      let a = i % 4
      let b = Math.floor(i / 4)
      v.x = a * (v.width + 9)
      v.y = b * (v.height + 30)
      this.addSubview(v)
    })

  }

  createSingleView(info, index) {
      let view = new ViewKit.View(ViewKit.Rect.rect(0, 0, 59, 60))
      view.bgColor = 'white'
      view.cornerRadius = 5
      let image = new ViewKit.ImageView(ViewKit.getRes('moreGame/0' + (index + 1)), ViewKit.Rect.rect(0, 0, 40, 40))
      image.anchorY = 0
      image.position = new ViewKit.Point(view.width / 2, 5)
      image.cornerRadius = 2
      image.maskToBounds = true
      let label = new ViewKit.Label()
      label.anchorY = 0
      label.position = new ViewKit.Point(view.width / 2, image.rect.bottom + 8)
      label.text = info[1]
      label.textColor = '#666666'
      label.textAlign = 'center'
      label.setFont(10)
      view.addSubview(image)
      view.addSubview(label)
      return view
  }

  getRandomIndex() {
    var arr = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];

    var result = [ ];

    var ranNum = 4;

    for (var i = 0; i < ranNum; i++) {

      var ran = Math.floor(Math.random() * arr.length);

      result.push(arr[ran]);

      var center = arr[ran];

      arr[ran] = arr[arr.length - 1];

      arr[arr.length - 1] = center;

      arr = arr.slice(0, arr.length - 1);
    }

    return result
  }
}

import * as ViewKit from '../../libs/ViewKit/index'
import DataBus from '../../DataBus'
import DuckHead from '../Home/DuckHead'
import ADHelper from '../../Helper/ADHelper'

export default class PassGroupCompleteView extends ViewKit.View {
  static banner = ADHelper.createBannerAD(ADHelper.adConfig['banner_game'])
willMoveToSuperview(view) {
  if(view == null) {
    DataBus.share().showChaseView()
    return
  }
  PassGroupCompleteView.banner.show()
  wx.aldSendEvent("进入通关页面")
  DataBus.share().showOpenDataView(false)
}
  get duckImageView () {
    if (this._duckImageView == null) {
      let v = null
      v = new DuckHead(ViewKit.Rect.rect(0, 0, 100, 100))
      v.anchorY = 0
      v.position = new ViewKit.Point(this.topView.width / 2, 50)
      this._duckImageView = v
    }
    return this._duckImageView
  }

  get duckBgView () {
    if (this._duckBgView == null) {
      let v = null
      v = new ViewKit.View(null, ViewKit.Rect.rect)
      this._duckBgView = v
    }
    return this._duckBgView
  }

  get topView () {
    if (this._topView == null) {
      let v = null
      v = new ViewKit.View(ViewKit.Rect.rect(0, 0, 225, 225))
      v.anchorY = 1
      v.position = new ViewKit.Point(this.width / 2, this.height / 2)
      v.bgColor = null
      // v.cornerRadius = 10
      this._topView = v
    }
    return this._topView
  }

  get getCoinBtn () {
    if (this._getCoinBtn == null) {
      let v = null
      v = new ViewKit.Button(ViewKit.Rect.rect(0, 0, 183, 56))
      v.cornerRadius = 28
      v.bgColor = '#f9ad33'
      v.text = '双倍领取'
      v.label.textColor = 'white'
      v.anchorY = 0
      v.position = new ViewKit.Point(this.width / 2, this.topView.rect.bottom + 30)
      v.isHidden = DataBus.share().preProduct
      this._getCoinBtn = v
    }
    return this._getCoinBtn
  }


  get nextPassBtn () {
    if (this._nextPassBtn == null) {
      let v = null
      v = new ViewKit.Button(ViewKit.Rect.rect(0, 0, 183, 56))
      v.cornerRadius = 28
      // v.bgColor = '#f9ad33'
      v.text = '下一关'
      v.label.textColor = 'white'
      v.anchorY = 0
      v.position = new ViewKit.Point(this.width / 2, this.getCoinBtn.rect.bottom + 10)
      this._nextPassBtn = v
    }
    return this._nextPassBtn
  }

  get coinImageView () {
    if (this._coinImageView == null) {
      let v = null
      v = new ViewKit.ImageView(ViewKit.getRes('coin'), ViewKit.Rect.rect(0, 0, 22, 22))
      v.anchorX = 1
      v.position = new ViewKit.Point(this.awardTitle.x - 3, this.awardTitle.center.y)
      this._coinImageView = v
    }
    return this._coinImageView
  }
  get awardTitle () {
    if (this._awardTitle == null) {
      let v = null
      v = new ViewKit.Label(ViewKit.Rect.rect(0, 0, 133, 20))
      v.position = new ViewKit.Point(this.topView.width / 2, this.topView.height - 50)
      v.text = '恭喜获得50金币~'
      v.setFont(19)
      this._awardTitle = v
    }
    return this._awardTitle
  }

  get titleLabel () {
    if (this._titleLabel == null) {
      let v = null
      v = new ViewKit.Label(ViewKit.Rect.rect(0, 0, 133, 20))
      // v.position = new ViewKit.Point(this.topView.width / 2, 50)
      // v.text = '恭喜获得10金币~'
      v.textAlign = 'center'
      v.setFont(19)
      this._titleLabel = v
    }
    return this._titleLabel
  }

  setupUI() {
    this.bgColor = 'rgba(0, 0, 0, 0.7)'
    let views = [this.topView, this.getCoinBtn, this.nextPassBtn, this.titleLabel]
    views.map(x => {
      this.addSubview(x)
    })

    views = [this.duckImageView, this.awardTitle, this.coinImageView]

    views.map(x => {
      this.topView.addSubview(x)
    })

  }

  addActionTarget() {
    this.nextPassBtn.clickedBlock = () => {
      wx.aldSendEvent("通关页-下一关")
      PassGroupCompleteView.banner.hide()
      if (this.nextBlock) {
        this.nextBlock()
      }
    }

    this.getCoinBtn.clickedBlock = () => {
      wx.aldSendEvent("通关页-双倍领取")
      if (this.getCoinBlock) {
        this.getCoinBlock()
      }
    }
  }

  get dataModel () {
    return this._dataModel
  }

  set dataModel(value) {
    this._dataModel = value
    this.update(value)
  }

  update(model) {
    // this.topView.bgColor = model.desc
    this.duckImageView.imageSrc = model.pic2
    this.titleLabel.text = '新鸭鸭解锁:' + model.title + "! ~"
    this.titleLabel.position = new ViewKit.Point(this.width / 2, 100)

    this.duckImageView.updateModel(model)
  }
}
PassGroupCompleteView.banner.hide()

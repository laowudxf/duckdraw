import * as ViewKit from '../../libs/ViewKit/index'
import PassDuckItem from './PassDuckItem'
import PassDuckItemPass from './PassDuckItemPass'
import DataBus from '../../DataBus'

export default class PassGroup extends ViewKit.View {

  get titleImageView() {
    if(this._titleImageView == null) {
      this._titleImageView = new ViewKit.ImageView(ViewKit.getRes('pass_title_1'), ViewKit.Rect.rect(0, 0, this.width - 15, (this.width - 15) * 0.08))
      this._titleImageView.anchorY = 0
      this._titleImageView.position = new ViewKit.Point(this.width / 2, 0)
    }
    return this._titleImageView
  }

  get titleLabel() {
    if(this._titleLabel == null) {
      this._titleLabel = new ViewKit.Label(ViewKit.Rect.rect(0, 0, 100, 20))
      this._titleLabel.text = 'duck'
      this._titleLabel.position = new ViewKit.Point(this.width / 2, this.titleImageView.height / 2)
      this._titleLabel.textAlign = 'center'
    }
    return this._titleLabel
  }

  setupUI() {
    this.itemViews = {}

    this.itemWidth = 94
    this.itemHeight = 130
    this.colume = 3
    this.itemGap = (this.width - (this.colume * this.itemWidth)) / this.colume
    this.itemBaseY = this.titleImageView.rect.bottom + this.itemGap / 2
    this.itemBaseWidth = this.itemWidth + this.itemGap

    let views = [this.titleImageView, this.titleLabel]
    views.map(x => {
      this.addSubview(x)
    })
  }

  get dataModel() {
    return this._dataModel
  }

  set dataModel(value) {
    this._dataModel = value
    this.update(value)
  }

  update(model) {
    console.log(model);
    this.titleLabel.text = model.name

    for (let index in this.itemViews) {
      this.itemViews[v].removeFromParent()
    }
    model.info.map((x, index) => {
      let row = parseInt(index / this.colume)
      let colume = index % this.colume

      let v = new PassDuckItem(ViewKit.Rect.rect(this.itemGap / 2 + colume * this.itemBaseWidth, this.itemBaseY + row * (this.itemHeight + this.itemGap), this.itemWidth, this.itemHeight))
      v.state = DataBus.share().isPassGroupLock(x.id)
      let tap = new ViewKit.TapGestureRecognzer(v, (e) => {
        if (v.state == 1) {
          return
        }
        ViewKit.SoundHelper.playSoundWithName('button')
        if (this.itemClick) {
          this.itemClick(x)

        }
      })
      v.addGesture(tap)
      v.dataModel = x
      this.itemViews[index] = v
      this.addSubview(v)
    })

    let showRow = parseInt((model.info.length - 1) / this.colume) + 1
    let height = showRow * (this.itemHeight + this.itemGap) + this.titleImageView.height
    this.height = height
  }

  get passItemViews() {
    let r = []
    for (let value in this.itemViews) {
        r.push(this.itemViews[parseInt(value)])
    }
    return r
  }

  refresh() {
    this.dataModel.info.map((x, index) => {
      let v = this.itemViews[index]
      if (v) {
        v.state = DataBus.share().isPassGroupLock(x.id)
      }
    })
  }
}


import * as ViewKit from '../../libs/ViewKit/index'

export default class RandomMoreGameView_checkView extends ViewKit.View {
  setupUI() {
    let map = [
      ['wx32f39e398bd85f6d',"脑力挑战"],
      ['wx17c68d004a3e2685',"欢乐连连"],
      ['wxf945a1bc6393a957',"六角消除"],
      // ['wxbd66bb89ec2b9805',"来赚一亿"],
      ['wx98edb9fd36968d0a',"成语接龙"],
      ['wx03eb4405d98fd065',"每天步赚"],
      ['wx0154c51079b81af8',"皇上吉祥"],
      ['wxbe04dd8bfd5ad65c',"王牌狙击"],
      ['wx5857c62152010a85',"拆散情侣"],
      ['wx36b6c21b34074aa6',"头条视频"],
      ['wx8113af51e011f04c',"猜图大全"],
    ]

    this.count = 5
    this.gap = 10
    this.itemWidth = (this.width - (this.count * this.gap)) / this.count
    this.height = this.itemWidth + 15 + 15
    let arr = this.getRandomIndex()

    arr.map((index, i) => {
      let x = map[index]

      // let v = new ViewKit.Button(ViewKit.Rect.rect(0, 0, 64, 64))
      let v = this.createSingleView(x, index)
      let tap = new ViewKit.TapGestureRecognzer(v, () => {
        wx.navigateToMiniProgram({appId: x[0]})
      })
      v.addGesture(tap)
      let count = this.count
      let a = i % count
      let b = Math.floor(i / count)
      v.x = a * (v.width + 10) + 5
      v.y = b * (v.height + 30) + 10
      this.addSubview(v)
    })

    this.cornerRadiusArr = [5, 5, 0, 0]
    this.borderWidth = 2
    this.borderColor = '#88c8e8'
    this.bgColor = '#ddedf5'

  }

  createSingleView(info, index) {
      let view = new ViewKit.View(ViewKit.Rect.rect(0, 0, this.itemWidth, this.itemWidth + 15))
      view.cornerRadius = 5
      let image = new ViewKit.ImageView(ViewKit.getRes('moreGame/0' + (index + 1)), ViewKit.Rect.rect(0, 0, this.itemWidth, this.itemWidth))
      image.anchorY = 0
      image.position = new ViewKit.Point(view.width / 2, 5)
      image.cornerRadius = image.height / 2
      image.maskToBounds = true
      let label = new ViewKit.Label()
      label.anchorY = 0
      label.position = new ViewKit.Point(view.width / 2, image.rect.bottom + 8)
      label.text = info[1]
      label.textColor = '#666666'
      label.textAlign = 'center'
      label.setFont(10)
      view.addSubview(image)
      view.addSubview(label)
      return view
  }

  getRandomIndex() {
    var arr = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];

    var result = [ ];

    var ranNum = this.count;

    for (var i = 0; i < ranNum; i++) {

      var ran = Math.floor(Math.random() * arr.length);

      result.push(arr[ran]);

      var center = arr[ran];

      arr[ran] = arr[arr.length - 1];

      arr[arr.length - 1] = center;

      arr = arr.slice(0, arr.length - 1);
    }

    return result
  }
}

import * as ViewKit from '../../libs/ViewKit/index'
import DuckHead from '../Home/DuckHead'

export default class PassDuckItem extends ViewKit.View {

  get titleLabel() {
    if(this._titleLabel == null) {
      this._titleLabel = new ViewKit.Label(ViewKit.Rect.rect(0, 0, 50, 14))
      this._titleLabel.text = '鸭子'
      this._titleLabel.setFont(14, 'bold')
      this._titleLabel.anchorY = 1
      this._titleLabel.position = new ViewKit.Point(this.width / 2, this.height - 5)
      this._titleLabel.textAlign = 'center'
      this._titleLabel.textColor = '#c4c9ca'
    }
    return this._titleLabel
  }

  get duckImageBgView() {
    if(this._duckImageBgView == null) {
      // this._duckImageBgView = new ViewKit.ImageView(ViewKit.getRes('blue_duck'), ViewKit.Rect.rect(0, 0, 70, 95))
      this._duckImageBgView = new DuckHead(ViewKit.Rect.rect(0, 0, 70, 70))
      this._duckImageBgView.autoAnimation = false
      this._duckImageBgView.anchorY = 0
      this._duckImageBgView.position = new ViewKit.Point(this.width / 2, 20)
      // this._duckImageBgView.bgColor = 'black'
    }
    return this._duckImageBgView
  }

  get bottomView () {
    if (this._bottomView == null) {
      let v = null
      v = new ViewKit.View(ViewKit.Rect.rect(0, 0, 60, 14))
      v.anchorY = 0
      v.anchorX = 0.5
      v.position = new ViewKit.Point(this.width / 2, this.duckImageBgView.rect.bottom)
      v.bgColor = '#e2e7e8'
      v.cornerRadius = 7
      this._bottomView = v
    }
    return this._bottomView
  }

  // get duckImageView() {
  //   if(this._duckImageView == null) {
  //     // this._duckImageView = new ViewKit.ImageView(ViewKit.getRes('blue_duck'), ViewKit.Rect.rect(0, 0, 70, 95))
  //     this._duckImageView = new ViewKit.ImageView(null, ViewKit.Rect.rect(0, 0, 70, 70 * 0.87))
  //     this._duckImageView.anchorY = 1
  //     this._duckImageView.anchorX = 0
  //     this._duckImageView.position = new ViewKit.Point(0, this._duckImageBgView.height + 10)
  //   }
  //   return this._duckImageView
  // }

  get duckImageView_lock() {
    if(this._duckImageView_lock == null) {
      this._duckImageView_lock = new ViewKit.ImageView(ViewKit.getRes('pass_duck_lock'), ViewKit.Rect.rect(0, 0, 71, 78))
      // this._duckImageView_lock = new ViewKit.ImageView(null, ViewKit.Rect.rect(0, 0, 70, 70 * 0.87))
      this._duckImageView_lock.anchorY = 1
      this._duckImageView_lock.position = new ViewKit.Point(this.width / 2, this.duckImageBgView.rect.bottom + 8)

      this._duckImageView_lock.isHidden = true
    }
    return this._duckImageView_lock
  }

  setupUI() {
    // 0 => ok , 1 => lock
    this.applyModel = false
    this.state = 1
    this.bgColor = 'white'
    this.cornerRadius = 22
    this.borderWidth = 6
    this.borderColor = '#dde3ee'
    let views = [this.bottomView, this.titleLabel, this.duckImageBgView, this.duckImageView_lock]
    views.map(x => {
      this.addSubview(x)
    })
  }

  get dataModel() {
    return this._dataModel
  }

  set dataModel(value) {
    this._dataModel = value
    this.update(value)
  }

  get state () {
    return this._state
  }

  set state(value) {
    this._state = value
    this.duckImageBgView.isHidden = value
    this.duckImageView_lock.isHidden = !value

    if (this.applyModel == false && this.duckImageBgView.isHidden == false && this.dataModel) {
      this.applyModel = true
      this.duckImageBgView.updateModel(this.dataModel)
    }

    if (this.dataModel) {
      this.titleLabel.text = this.state ? '未解锁' : this.dataModel.title
      // this.titleLabel.textColor = this.state ? '#999999' : this.dataModel.desc
    }
  }

  update(model) {
    this.titleLabel.text = this.state ? '未解锁' : model.title
    if (this.state == 0) {
      this.applyModel = true
      this.duckImageBgView.updateModel(model)
    }
  }
}

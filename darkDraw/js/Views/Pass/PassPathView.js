import * as ViewKit from '../../libs/ViewKit/index'

class PassPathDrawView extends ViewKit.View {
  setupUI() {
    this.piexlScale = ViewKit.Device.share().scale

  }

  get baseWidth () {
    return this._baseWidth
  }

  set baseWidth(value) {
    this._baseWidth = value
  }

  get passPathData () {
    return this._passPathData
  }

  set passPathData(value) {
    this._passPathData = value
  }

  draw(ctx, rect) {
    super.draw(ctx, rect)
    if (this.passPathData == null) {
      return
    }
    // let drawRectArr = this.clacDrawRect(rect)

    ctx.beginPath()
    ctx.strokeStyle = this.themeColor ? this.themeColor : 'black'
    ctx.lineWidth = (this.baseWidth - 1) * this.piexlScale
    ctx.lineCap = 'square'
    let paths = this.passPathData.passPath
    paths.map((info , i) => {
      let point = new ViewKit.Point(info.colume * this.baseWidth + this.baseWidth / 2, info.row * this.baseWidth + this.baseWidth / 2)
      point = this.calcDrawPoint(rect, point)
      if (i = 0) {
        ctx.moveTo(point.x, point.y)
      } else {
        ctx.lineTo(point.x, point.y)
      }
    })
    // for (let i = 0; i < paths.length; i++) {
    // //
    // }
    ctx.stroke()
    ctx.beginPath()
  }

  calcDrawPoint(rect, point) {
    point.x += rect.x
    point.y += rect.y
    if (this.currentCtx.drawOffset) {
      point.x += this.currentCtx.drawOffset.x
      point.y += this.currentCtx.drawOffset.y
    }
    point.x *= this.piexlScale
    point.y *= this.piexlScale
    return point
  }
}

export default class PassPathView extends ViewKit.View {

  setupUI() {
    this.baseWidth = this.width / 9
    this.bgColor = '#f1f1f1'
    this.cornerRadius = 10
  }

  get passPathData () {
    return this._passPathData
  }

  set passPathData(value) {
    this._passPathData = value
    this.createDrawView()
  }

  get themeColor () {
    return this._themeColor
  }

  set themeColor(value) {
    this._themeColor = value
  }

  get passDrawView () {
    if (this._passDrawView == null) {
      let v = null
      v = new PassPathDrawView(ViewKit.Rect.rect(0, 0, this.baseWidth * this.passPathData.colume, this.baseWidth * this.passPathData.row))
      v.baseWidth = this.baseWidth
      v.position = new ViewKit.Point(this.width / 2, this.height / 2)
      v.passPathData = this.passPathData
      v.themeColor = this.themeColor
      this._passDrawView = v
    }
    return this._passDrawView
  }

  createDrawView() {
    if (this.passDrawView.parentView) {
      this.passDrawView.removeFromParent()
      this._passDrawView = null
    }

    this.addSubview(this.passDrawView)

  }

  //rect 绝对位置
  // draw(ctx, rect) {
  //   super.draw(ctx, rect)
  //   if (this.passPathData == null) {
  //     return
  //   }
  //   ctx.beginPath()
  //
  // }

}

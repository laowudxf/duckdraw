import * as ViewKit from '../../../libs/ViewKit/index'
import DailyTimeLabel from './DailyTimeLabel'
import DataBus from '../../../DataBus'

export default class DailyTimeController extends ViewKit.ViewController {

  get couldBeClick() {
    return this.view.isHidden
  }

  calc() {
      let last = DataBus.share().lastGetDailyRewardTime
      if (last == null) {
        this.view.isHidden = true
        return
      }
      let now = Date.now()

      let offset = now - last

      if (offset > (1000 * 3600)) { //大于一小时
        this.view.isHidden = true
        return
      }

      offset = 3600 * 1000 - offset
      this.calcTimeStr(offset)

      this.view.isHidden = false

  }
  startCount() {
    this.calc()
    this.intervalId = setInterval(() => {
      this.calc()
    }, 1000)
  }

  stopCount() {
    if (this.intervalId) {
        clearInterval(this.intervalId)
    }

  }

  loadView() {
    this.view = new DailyTimeLabel()
    this.view.textAlign = 'center'
    this.view.text = '00:00:00'
    this.view.textColor = '#666666'
    this.view.setFont(15, 'bold')
  }

  calcTimeStr(offset) {
    offset = Math.floor(offset / 1000)
    let sec = offset % 60
    let min = Math.floor(offset / 60) % 60
    let hour = Math.floor(offset / 3600) % 24
    this.view.text = this.timeFormate(hour) + ':' + this.timeFormate(min) + ':' + this.timeFormate( sec)
  }

  timeFormate(str) {
    str = str + ''
    if (str.length == 1) {
      return ('0' + str)
    }
    return str
  }

}

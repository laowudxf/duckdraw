import * as ViewKit from '../libs/ViewKit/index'
import HomeView from './HomeView'
import PassGroup from './Pass/PassGroup'
import EndPoint from '../Helper/EndPoint'
import DataBus from '../DataBus'
import RandomMoreGameView_checkView from './Pass/RandomMoreGameView_checkView'

export default class CheckPointView extends ViewKit.View {

  get moreGame () {
    if (this._moreGame == null) {
      let v = null
      v = new RandomMoreGameView_checkView(ViewKit.Rect.rect(0, 0, this.width - 40, 80))
      v.anchorY = 1
      v.position = new ViewKit.Point(this.width / 2, this.height)
      this._moreGame = v
    }
    return this._moreGame
  }

  get backBtn() {
    if(this._backBtn == null) {
      let y = 17
      if (ViewKit.Device.share().isPhoneX) {
         y += 10
      }
      this._backBtn = new ViewKit.Button(ViewKit.Rect.rect(0, y, 72, 41.5))
      this._backBtn.text = ''

      this._backBtn.bgImage = ViewKit.getRes('btn_back')
    }
    return this._backBtn
  }

  get titleImageView() {
    if (this._titleImageView == null) {
      this._titleImageView = new ViewKit.ImageView(ViewKit.getRes('pass_title'), ViewKit.Rect.rect(0, 0, 94, 22))
      this._titleImageView.position = new ViewKit.Point( this.width / 2 ,this.backBtn.center.y)
    }
    return this._titleImageView
  }

  randomHexColor() { //随机生成十六进制颜色
    var hex = Math.floor(Math.random() * 16777216).toString(16); //生成ffffff以内16进制数
    while (hex.length < 6) { //while循环判断hex位数，少于6位前面加0凑够6位
      hex = '0' + hex;
    }
    let r = '#' + hex; //返回‘#'开头16进制颜色
    return r
  }

  setupUI() {
    this.groupViews = []
    this.bgColor = 'white'
    let views = [this.backBtn, this.titleImageView, this.moreGame]
    views.map((x) => this.addSubview(x))
    let container = new ViewKit.ScrollView(ViewKit.Rect.rect(0, 100, this.width, this.height - this.moreGame.height - 100))
    this.scrollView = container
    this.addSubview(container)

    EndPoint.pass()
    .then(data => {
      DataBus.share().passData = data.data.data
      this.passData = data.data.data
    })
    // this.passData = DataBus.share().passData
  }

  get passData() {
    return this._passData
  }

  set passData(value) {
    this._passData = value
    this.updatePass(value)
  }

  updatePass(data) {
    console.log(data);
    let last = null
    this.groupViews.map(x => x.removeFromParent())
    data.map((x, index) => {
      let v = new PassGroup(ViewKit.Rect.rect(0, last ? last.rect.bottom : 0, this.width, 100))
      v.dataModel = x
      this.groupViews.push(v)
      last = v
      this.scrollView.contentView.addSubview(v)
      if (this.itemClick) {
        v.itemClick = (dataModel) => {
          this.itemClick(dataModel)
        }
      }
    })

    let r = this.groupViews[this.groupViews.length - 1].rect
    this.scrollView.contentView.height = r.bottom
  }

  refresh() {
      this.groupViews.map(x => x.refresh())
  }

}

import * as ViewKit from '../../libs/ViewKit/index'
import RankBoard from '../RankBoard/RankBoard'

export default class RankBoardViewController extends ViewKit.ViewController{

  loadView() {
    this.view = new RankBoard(this.screenRect)
  }
  viewWillAppear() {
    wx.aldSendEvent("进入排行榜页面")
  }

  addActionTarget() {
    this.view.closeBtn.clickedBlock = () => {
    wx.aldSendEvent("排行页-关闭")

      this.parentContainer.pop()
    }

  }
}

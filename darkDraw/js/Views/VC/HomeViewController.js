import * as ViewKit from '../../libs/ViewKit/index'
import HomeView from '../HomeView'
import RankBoardViewController from './RankBoardViewController'
import CheckPointViewController from './CheckPointViewController'
import DailyRewardView from '../Home/DailyRewardView'
import CollectTipView from '../Home/CollectTipView'
import ADHelper from '../../Helper/ADHelper'
import DailyTimeController from '../CustomView/DailyTimeLabel/DailyTimeController'
import User from '../../DataModel/User'
import MoreGameView from '../Home/MoreGameView'
import PassDuckItemPassViewController from './PassDuckItemPassViewController'
import GameDrawViewController from './GameDrawViewController'
import DataBus from '../../DataBus'


import Fireworks from '../Particle/Fireworks'
export default class HomeViewController extends ViewKit.ViewController{

  get dailyTimeController () {
    if (this._dailyTimeController == null) {
      let v = null
      v = new DailyTimeController()
      v.view.anchorY = 0
      v.view.anchorX = 0.5
      this._dailyTimeController = v
    }
    return this._dailyTimeController
  }

  get bannerADView () {
    if (this._bannerADView == null) {
      let v = null
      v = new ViewKit.View(ViewKit.Rect.rect(0, 0, this.view.width, 120))
      v.anchorX = 0
      v.anchorY = 1
      v.position = new ViewKit.Point(0, this.view.height)
      v.bgColor = null
      this._bannerADView = v
    }
    return this._bannerADView
  }

  loadView() {
    this.view = new HomeView(ViewKit.App.share().window.bounds)
  }

  subscribe(before, now, key, instance) {
    switch (key) {
      case 'userInfo':
      if (now) {
        this.checkPointViewController.view
        User.share().removeObserver(this)
      }
      break
      default:
      break
    }
  }

  viewWillAppear() {
    if (this.bannerAd) {
      this.bannerAd.hide()
      this.bannerAd.destory()
    }
    this.bannerAd = ADHelper.createBannerAD(ADHelper.adConfig['banner_home'])
    wx.aldSendEvent("进入游戏首页")
  }

  viewWillDisappear() {
    if (this.bannerAd) {
      this.bannerAd.hide()
      this.bannerAd.destory()
      this.bannerAd = null
    }

  }

  get testBtn () {
    if (this._testBtn == null) {
      let v = null
      v = new ViewKit.Button(ViewKit.Rect.rect(0, 0, 100 , 30))
      v.position = this.view.bounds.center
      v.bgColor = 'red'
      v.isHidden = true
      this._testBtn = v
    }
    return this._testBtn
  }

  setupUI() {
    // bannerAd.show()
    this.view.addSubview(this.testBtn)
    this.addChild(this.dailyTimeController)
    this.dailyTimeController.startCount()
    this.dailyTimeController.view.position = new ViewKit.Point(this.view.dailyReward.rect.center.x, this.view.dailyReward.rect.bottom + 15)
    User.share().registerObserver(this, 'userInfo')

    // this.view.addSubview(this.bannerADView)

    let tmpx = this.view.bgImageView.x
    let tmpy = this.view.bgImageView.y
    ViewKit.Animation.get(this.view.bgImageView, {loop: true})
    .to({'translation.x': -(83 / ViewKit.Device.share().scale), 'translation.y':  (83 / ViewKit.Device.share().scale)}, 1500)
    .call(() => {
      this.translation = new ViewKit.Point(0, 0)
    })
    .start()
  }

  get checkPointViewController () {
    if (this._checkPointViewController == null) {
      let v = null
      v = new CheckPointViewController()
      this._checkPointViewController = v
    }
    return this._checkPointViewController
  }

  addActionTarget() {
    this.testBtn.clickedBlock = () => {
      let v = new Fireworks(ViewKit.Rect.rect(0, 0, 100, 100))
      v.position = this.view.bounds.center
      this.view.addSubview(v)
    }

    this.view.giftBtn.clickedBlock = () => {
      this.bannerAd.hide()
      let v = new CollectTipView(this.view.bounds)
      v.closeBtn.clickedBlock = () => {
        v.removeFromParent()
        this.bannerAd.show()

      }
      this.view.addSubview(v)
    }

    this.view.moreBtn.clickedBlock = () => {
        let v = new MoreGameView(this.view.bounds)
        this.bannerAd.hide()
        v.closeBtn.clickedBlock = () => {
        this.bannerAd.show()
          v.removeFromParent()
        }
        this.view.addSubview(v)
    }


    this.view.playBtn.clickedBlock = () => {
      wx.aldSendEvent("开始游戏")
      this.parentContainer.push(this.checkPointViewController)
      let gameProcess = User.share().gameProcessPosition
      if (gameProcess) {
        let vc = new PassDuckItemPassViewController()

        let model = DataBus.share().catModelWithId(gameProcess.cat_id)
        console.log(model);
        vc.dataModel = model.model
        vc.refreshBlock = () => {
          this.refresh()
        }
        this.parentContainer.push(vc)

        let vc_1 = new GameDrawViewController()
        vc_1.passGroupItemProupVC = vc
        vc_1.dataModel = model.model
        vc_1.index = gameProcess.index
        this.parentContainer.push(vc_1)
      }

    }

    this.view.rankBtn.clickedBlock = () => {
        wx.aldSendEvent("排行榜")
      let vc = new RankBoardViewController()
      this.parentContainer.push(vc)
      // this.addSubview(v)
    }

    this.view.dailyReward.clickedBlock = () => {
      if (this.dailyTimeController.couldBeClick == false) {
      wx.aldSendEvent("每日奖励", "失败-时间未到")
        return
      }
      wx.aldSendEvent("每日奖励")
      let v = new DailyRewardView(this.view.bounds)
      wx.aldSendEvent("进入每日登陆奖励页面")
      this.view.addSubview(v)
    }

    // let bannerTap = new ViewKit.TapGestureRecognzer(this.bannerADView, (ges) => {
    // })
    // this.bannerADView.addGesture(bannerTap)
  }
}


import * as ViewKit from '../../libs/ViewKit/index'
import CheckPointView from '../CheckPointView'
import PassDuckItemPassViewController from './PassDuckItemPassViewController'

export default class CheckPointViewController extends ViewKit.ViewController{

  loadView() {
    this.view = new CheckPointView(ViewKit.App.share().window.bounds)
  }

  setupUI() {
    this.hadStartAnimation = false
  }

  viewDidAppear() {
    wx.aldSendEvent("进入关卡页面")
    if (this.hadStartAnimation == false) {
      this.hadStartAnimation = true
      this.view.groupViews.map(x => {
        x.passItemViews.map(a => {
          a.duckImageBgView.animation()
        })
      })
    }
  }


  // viewWillDisappear() {
  //     this.view.groupViews.map(x => {
  //       x.passItemViews.map(a => {
  //         a.duckImageBgView.stopAnimation()
  //       })
  //     })
  //   }

  addActionTarget() {
        // let detail = new PassDuckItemPass(ViewKit.App.share().window.bounds)
        // detail.dataModel = x
        // ViewKit.App.share().rootView = detail

      this.view.itemClick = (model) => {
        wx.aldSendEvent("关卡页关卡选择",{"关卡名": model.title})
        let vc = new PassDuckItemPassViewController()
        vc.dataModel = model
        vc.refreshBlock = () => {
          this.refresh()
        }
        this.parentContainer.push(vc)
      }

    this.view.backBtn.clickedBlock = () => {
      console.log(1111);
    wx.aldSendEvent("关卡页返回", '关卡页面')
      this.parentContainer.pop()
    }
  }

  refresh() {
    this.view.refresh()
  }

}

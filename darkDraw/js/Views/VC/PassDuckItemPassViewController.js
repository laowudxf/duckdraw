
import * as ViewKit from '../../libs/ViewKit/index'
import GameDrawViewController from './GameDrawViewController'
import PassDuckItemPass from '../Pass/PassDuckItemPass'

export default class PassDuckItemPassViewController extends ViewKit.ViewController{

  viewWillAppear() {
    wx.aldSendEvent("进入子关卡页面")
  }

  setupUI() {

  }

  addActionTarget() {
    this.view.backBtn.clickedBlock = () => {
    wx.aldSendEvent("子关卡页返回", "子关卡页面")
      this.parentContainer.pop()
    }

    this.view.itemClick = (model, index) => {
      wx.aldSendEvent("子关卡页子关卡选择")
      let vc = new GameDrawViewController()
      vc.passGroupItemProupVC = this
      console.log(model);
      vc.dataModel = model
      vc.index = index
      this.parentContainer.push(vc)

        // let drawView = new GameDrawView(this.bounds)
        // drawView.dataModel = this.dataModel
        // drawView.index = index
        // ViewKit.App.share().rootView = drawView
    }
  }

  loadView() {
    this.view = new PassDuckItemPass(this.screenRect)
    this.view.dataModel = this.dataModel
  }

  get dataModel () {
    return this._dataModel
  }

  set dataModel(value) {
    this._dataModel = value
    if (this._view) {
      this._view.dataModel = value
      if (this.refreshBlock) {
        this.refreshBlock()
      }
    }
  }
}

import * as ViewKit from '../../libs/ViewKit/index'
import GameDrawView from '../Draw/GameDrawView'
import User from '../../DataModel/User'
import DataBus from '../../DataBus'
import Service from '../../Service/Service'
import PassGroupCompleteView from '../Pass/PassGroupCompleteView'
import PassCompleteView from '../Pass/PassCompleteView'
import ShareHelper from '../../Helper/ShareHelper'
import RewardView from '../Draw/RewardView'
import ADHelper from '../../Helper/ADHelper'
import DailyTimeController from '../CustomView/DailyTimeLabel/DailyTimeController'
import DailyRewardView from '../Home/DailyRewardView'
import RankBoardViewController from './RankBoardViewController'
import MoreGameView from '../Home/MoreGameView'

export default class GameDrawViewController extends ViewKit.ViewController{
  get dailyTimeController () {
    if (this._dailyTimeController == null) {
      let v = null
      v = new DailyTimeController()
      v.view.anchorY = 0
      v.view.anchorX = 0.5
      this._dailyTimeController = v
    }
    return this._dailyTimeController
  }


  viewDidAppear() {
    wx.aldSendEvent("进入闯关页面")
  }

  viewDidAppear() {
  }

  viewWillDisappear() {
    DataBus.share().showOpenDataView(false)
  }

  get fingerView () {
    if (this._fingerView == null) {
      let v = null
      v = new ViewKit.ImageView(ViewKit.getRes('finger'), ViewKit.Rect.rect(0, 0, 122, 103))
      v.anchorX = 0
      v.anchorY = 0.2
      v.position = new ViewKit.Point(this.view.gameBoardView.x, this.view.height / 2)
      this._fingerView = v
    }
    return this._fingerView
  }

  setupUI() {

    this.addChild(this.dailyTimeController)
    this.dailyTimeController.startCount()
    this.dailyTimeController.view.position = new ViewKit.Point(this.view.awardBtn.rect.center.x, this.view.awardBtn.rect.bottom + 15)

    if (this.dataModel.id == 123 && this.index == 0) {
      this.setupCourse()
    }
  }

  setupCourse() {
    this.view.addSubview(this.fingerView)

    ViewKit.Animation.get(this.fingerView, {loop: true})
    .wait(400)
    .to({'translation.x': this.view.gameBoardView.width}, 2000)
    .wait(400)
    .call(() => {
      this.fingerView.translation.x = 0
    })
    .start()
    this.view.gameBoardView.clickTip()

  }

  addActionTarget() {
    this.view.moreBtn.clickedBlock = () => {
        let v = new MoreGameView(this.view.bounds)
        v.closeBtn.clickedBlock = () => {
          v.removeFromParent()
        }
        this.view.addSubview(v)
    }

      this.view.tipBtn.clickedBlock = () => {
        if (this.view.couldTip == false) {
          return
        }
        if (Service.share().tip() == -1) {

          if (DataBus.share().preProduct) {
            wx.showModal({
              title: '提示',
              content: '暂未开放',
              showCancel: false
            })
            return
          }
          wx.aldSendEvent("闯关页-奖励金币页-观看视频")
          let video = ADHelper.createVideoAd(ADHelper.adConfig['video_reward'], () => {
            User.share().coin += 50

            wx.showModal({
              title: '提示',
              content: '+50金币',
              showCancel: false
            })

          }, () => {

          })

          ADHelper.showVideoAd(video)
          return
        }

        wx.aldSendEvent("闯关页-提示", "成功")
        this.view.coinReduceAnimation()
        this.view.gameBoardView.clickTip()
      }

    this.view.backBtn.clickedBlock = () => {
      wx.aldSendEvent("闯关页-返回", '游戏页面')
      if (this.parentContainer) {
        this.parentContainer.pop()
      }
    }

    this.view.awardBtn.clickedBlock = () => {
      if (this.dailyTimeController.couldBeClick == false) {
      wx.aldSendEvent("闯关页-宝箱", "失败-时间未到")
        return
      }

      wx.aldSendEvent("闯关页-宝箱")
      DataBus.share().showOpenDataView(false)
      let v = new DailyRewardView(this.view.bounds)
      v.closeBlock = () => {
        DataBus.share().showChaseView()
      }
      this.view.addSubview(v)

    }

    this.view.coinAddBtn.clickedBlock = () => {
      wx.aldSendEvent("闯关页-金币加号")
      if (DataBus.share().preProduct) {
        wx.showModal({
          title: '提示',
          content: '暂未开放',
          showCancel: false
        })
        return
      }
        wx.aldSendEvent("闯关页-奖励金币页-观看视频")
      let video = ADHelper.createVideoAd(ADHelper.adConfig['video_reward'], () => {
        User.share().coin += 50

        wx.showModal({
          title: '提示',
          content: '+50金币',
          showCancel: false
        })

      }, () => {

      })

      ADHelper.showVideoAd(video)
    }
    this.view.tipFreeBtn.clickedBlock = () => {
      this.videoAd = ADHelper.createVideoAd(ADHelper.adConfig['video_free_tip'], () => {
        console.log('success');
      wx.aldSendEvent("闯关页-免费提示", "成功")
        this.view.gameBoardView.clickTip()
      }, () => {
      wx.aldSendEvent("闯关页-免费提示", "失败")
        console.log('fail');
      })

      ADHelper.showVideoAd(this.videoAd)
      // wx.showModal({
      //   title: '提示',
      //   content: '暂未开放',
      //   showCancel: false
      // })
    }
  }

  loadView() {
    this.view = new GameDrawView(this.screenRect)
    this.view.dataModel = this.dataModel
    // console.log(this.dataModel, 11111111111);
    this.view.index = this.index
    this.view.winBlock = () => {
      this.winBlock()
    }
  }

  goNext(model, index, next) {
    let vc = new GameDrawViewController()
    vc.dataModel = model
    vc.index = index
    let parent = this.parentContainer
    this.parentContainer.pop()
    parent.push(vc)

    if (this.passGroupItemProupVC) {
      this.passGroupItemProupVC.dataModel = next.model
    }

  }

  winBlock() {
    //sound
    ViewKit.SoundHelper.playSoundWithName('pass')
    //save passPaths
    let passPath = this.view.gameBoardView.passPath()
    console.log(passPath);
    let userPassPaths = User.share().passPaths
    userPassPaths[this.dataModel.id + '_' + this.index] = passPath
    User.share().passPaths = userPassPaths


    //set highestScore
    let cat_id = this.dataModel.pass[this.index].cat_id;
    let countData = DataBus.share().catModelWithId(cat_id, this.index)
    let score = countData.count + this.index + 1
    // DataBus.postMessage({
    //   method: 'saveHighestScore',
    //   param: {
    //     score: score
    //   }
    // })
    // ------------

    let v = null
    let next = DataBus.share().nextCatModelWithId(this.dataModel.id, this.index)

    let isLastPass = (next.index == 0)
    if (isLastPass) {
      v = new PassGroupCompleteView(this.view.bounds)
      v.dataModel = next.model
    } else {
      v = new PassCompleteView(this.view.bounds)
      DataBus.share().showPreAfterView(score)
      v.rankBoard.clickedBlock = () => {
        let vc = new RankBoardViewController()
        this.parentContainer.push(vc)
      }
      v.dataModel = this.dataModel
    }



    let isValid = DataBus.share().isValidPassComplete(cat_id, this.index)
    v.getCoinBlock = () => {
      let video = ADHelper.createVideoAd(ADHelper.adConfig['video_reward'], () => {
        if (!isValid) {
          v.nextBlock()
          return
        }
        wx.showModal({
          title: '奖励来了',
          content: '领取金币' + (isLastPass ? '+100': '+20'),
          showCancel: false,
          success: () => {
            User.share().coin += isLastPass ? 50 : 10
            v.nextBlock()
          }
        })
      }, () => {
        wx.showModal({
          title: '双倍领取失败',
          content: '双倍领取失败',
          showCancel: false
        })
      })

      ADHelper.showVideoAd(video)

      // ShareHelper.share(4)
      // .then(() => {
      //   if (!isValid) {
      //     v.nextBlock()
      //     return
      //   }
      //   wx.showModal({
      //     title: '奖励来了',
      //     content: '领取金币' + (isLastPass ? '+100': '+20'),
      //     showCancel: false,
      //     success: () => {
      //       User.share().coin += isLastPass ? 50 : 10
      //       v.nextBlock()
      //     }
      //   })
      // }).catch(() => {
      //   wx.showModal({
      //     title: '分享失败',
      //     content: '需要分享到不同的群',
      //     showCancel: false
      //   })
      // })
    }

    v.nextBlock = () => {
      Service.share().passComplete(cat_id, this.index)
      if (isValid) {
        User.share().coin += isLastPass ? 50 : 10
      }
      let v = new GameDrawView(this.bounds)
      if (!next) {
        return
      }
      this.goNext(next.model, next.index, next)

    }
    this.view.addSubview(v)
  }
}

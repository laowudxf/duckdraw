
import * as ViewKit from '../../libs/ViewKit/index'
import ShareHelper from '../../Helper/ShareHelper'
import Service from '../../Service/Service'
import User from '../../DataModel/User'
import DataBus from '../../DataBus'
import ADHelper from '../../Helper/ADHelper'

export default class RewardView extends ViewKit.View {
  get titleLabel () {
    if (this._titleLabel == null) {
      let v = null
      v = new ViewKit.Label(ViewKit.Rect.rect(0, 0, 100, 30))
      v.text = '奖励金币'
      v.textColor = 'white'
      v.textAlign = 'center'
      v.setFont(25)
      v.anchorY = 0
      v.position = new ViewKit.Point(this.width / 2, 100)
      this._titleLabel = v
    }
    return this._titleLabel
  }

  get titleDesc () {
    if (this._titleDesc == null) {
      let v = null
      v = new ViewKit.Label(ViewKit.Rect.rect(0, 0, 100, 20))
      v.text = '动动手指，就可以获得金币奖励鸭~'
      v.textColor = 'white'
      v.textAlign = 'center'
      v.setFont(14)
      v.anchorY = 0
      v.position = new ViewKit.Point(this.width / 2, this.titleLabel.rect.bottom)
      this._titleDesc = v
    }
    return this._titleDesc
  }

  get shareGroup () {
    if (this._shareGroup == null) {
      let v = null
      v = new ViewKit.Button(ViewKit.Rect.rect(0, 0, 96, 96))
      v.bgColor = '#f8dc66'
      v.cornerRadius = 96 / 2
      v.anchorY = 0
      v.position = new ViewKit.Point(this.width / 2 - 70, this.titleDesc.rect.bottom + 50)
      v.label.text = '分享到群'
      v.label.translation = new ViewKit.Point(0, 70)
      v.label.textColor = 'white'
      this._shareGroup = v
    }

    return this._shareGroup
  }

  get shareGroupImageView () {
    if (this._shareGroupImageView == null) {
      let v = null
      v = new ViewKit.ImageView(ViewKit.getRes('share'), ViewKit.Rect.rect(0, 0, 50, 50))
      v.position = this.shareGroup.bounds.center
      this._shareGroupImageView = v
    }
    return this._shareGroupImageView
  }

  get groupCoinImage () {
    if (this._groupCoinImage == null) {
      let v = null
      v = new ViewKit.ImageView(ViewKit.getRes('coin'), ViewKit.Rect.rect(0, 0, 25, 25))
      v.anchorX = 1
      v.position = new ViewKit.Point(this.shareGroupCoin.x - 5, this.shareGroupCoin.center.y)
      this._groupCoinImage = v
    }
    return this._groupCoinImage
  }

  get shareGroupCoin () {
    if (this._shareGroupCoin == null) {
      let v = null
      v = new ViewKit.Label(ViewKit.Rect.rect(0, 0, 30, 20))
      v.text = 'X50'
      v.textColor = 'white'
      v.setFont(14)
      v.anchorY = 0
      v.position = new ViewKit.Point(this.width / 2 + 20  - 70, this.shareGroup.rect.bottom + 50)
      this._shareGroupCoin = v
    }
    return this._shareGroupCoin
  }

  get lookVideo () {
    if (this._lookVideo == null) {
      let v = null
      v = new ViewKit.Button(ViewKit.Rect.rect(0, 0, 96, 96))
      v.bgColor = '#b5dc44'
      v.cornerRadius = 96 / 2
      v.anchorY = 0
      v.position = new ViewKit.Point(this.width / 2 + 70, this.titleDesc.rect.bottom + 50)
      v.label.text = '观看视频'
      v.label.translation = new ViewKit.Point(0, 70)
      v.label.textColor = 'white'
      this._lookVideo = v
    }

    return this._lookVideo
  }

get videoImageView () {
  if (this._videoImageView == null) {
    let v = null
    v = new ViewKit.ImageView(ViewKit.getRes('video'), ViewKit.Rect.rect(0, 0, 50, 50))
    v.position = this.lookVideo.bounds.center
    this._videoImageView = v
  }
  return this._videoImageView
}

get videoCoinImage () {
  if (this._videoCoinImage == null) {
    let v = null
    v = new ViewKit.ImageView(ViewKit.getRes('coin'), ViewKit.Rect.rect(0, 0, 25, 25))
    v.anchorX = 1
    v.position = new ViewKit.Point(this.videoCoinLabel.x - 5, this.videoCoinLabel.center.y)
    this._videoCoinImage = v
  }
  return this._videoCoinImage
}

get videoCoinLabel () {
  if (this._videoCoinLabel == null) {
    let v = null
    v = new ViewKit.Label(ViewKit.Rect.rect(0, 0, 30, 20))
    v.text = 'X50'
    v.textColor = 'white'
    v.setFont(14)
    v.anchorY = 0
    v.position = new ViewKit.Point(this.width / 2 + 20 + 70, this.lookVideo.rect.bottom + 50)
    this._videoCoinLabel = v
  }
  return this._videoCoinLabel
}

get closeBtn () {
  if (this._closeBtn == null) {
    let v = null
    v = new ViewKit.Button(ViewKit.Rect.rect(0, 0, 32, 32))
    v.bgImage = ViewKit.getRes('close')
    v.anchorY = 0
    v.position = new ViewKit.Point(this.width / 2, this.groupCoinImage.rect.bottom + 60)
    v.text = ''
    this._closeBtn = v
  }
  return this._closeBtn
}

willMoveToSuperview(view) {
  if(view == null) {
    DataBus.share().showChaseView()
    return
  }
  DataBus.share().showOpenDataView(false)
}

  setupUI() {
    this.bgColor = 'rgba(0,0,0,0.7)'
    let views = [this.titleLabel, this.titleDesc, this.shareGroup, this.shareGroupCoin, this.groupCoinImage, this.closeBtn,
      this.lookVideo, this.videoCoinImage, this.videoCoinLabel]
    this.shareGroup.addSubview(this.shareGroupImageView)
    this.lookVideo.addSubview(this.videoImageView)
    views.map(x => this.addSubview(x))
  }

  addActionTarget() {
    this.closeBtn.clickedBlock = () => {
        wx.aldSendEvent("闯关页-奖励金币页-关闭")
      this.removeFromParent()
    }
    this.shareGroup.clickedBlock = () => {
        wx.aldSendEvent("闯关页-奖励金币页-分享到群")
      this.rewardShare()
    }

    this.lookVideo.clickedBlock = () => {
        wx.aldSendEvent("闯关页-奖励金币页-观看视频")
      let video = ADHelper.createVideoAd(ADHelper.adConfig['video_reward'], () => {
        User.share().coin += 50

        wx.showModal({
          title: '提示',
          content: '+50金币',
          showCancel: false
        })

      }, () => {

      })

      ADHelper.showVideoAd(video)

    }
  }

  rewardShare() {
    ShareHelper.share(4)
    .then(() => {
      User.share().coin += 50
      this.removeFromParent()
      wx.showModal({
        title: '奖励',
        content: '奖励50金币',
        showCancel: false
      })
    }).catch(err => {
      wx.showModal({
        title: '分享失败',
        content: '需要分享到不同的群',
        showCancel: false
      })
    })
  }
}

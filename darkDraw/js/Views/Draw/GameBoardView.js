import * as ViewKit from '../../libs/ViewKit/index'
import DuckHeadView from './DuckHeadView'
import BoardBlockView from './BoardBlockView'
import DuckPathView from './DuckPathView'
import DuckHead from '../Home/DuckHead'
import DuckTail from '../Home/DuckTail'

/*
* boardFlags 0 => 障碍
            1 => 可移动区域
            2 => 起始位置
            3 => 被选取
*/

class BoardItemModel {
  constructor() {
    this.row = null
    this.colume = null
    this.state = 1

    this.up = null
    this.down = null
    this.left = null
    this.right = null
  }

  setupAroundRelation(boardFlags) {
    let boardWidth = boardFlags[0].length
    let boardHeight = boardFlags.length
    let findItemModel= (location) => {
      if (location[0] >= boardWidth || location[1] >= boardHeight || location[0] < 0 || location[1] < 0) {
        return null
      }
      return boardFlags[location[1]][location[0]]
    }

    //up
    this.up = findItemModel([this.colume, this.row - 1])
    this.down = findItemModel([this.colume, this.row + 1])
    this.left = findItemModel([this.colume - 1, this.row])
    this.right = findItemModel([this.colume + 1, this.row])
  }

  validNextStepArr() {
      let result = [this.right, this.down, this.left, this.up]
      return result.filter(x => x != null && x.state != 0)
  }

  validNextStepNotBackArr(pathArr) {
      let result = [this.right, this.down, this.left, this.up]
      result = result.filter(x => x != null && x.state != 0)
      .filter(x => pathArr.indexOf(x) == -1)

      //一点点优化
      .sort((a, b) => {
        return a.validStepCount(pathArr) - b.validStepCount(pathArr)
      })

      return result
  }

  validStepCount(pathArr) {
    let result = [this.right, this.down, this.left, this.up]
    result = result.filter(x => x != null && x.state != 0 && pathArr.indexOf(x) == -1)
    return result.length
  }

}

export default class GameBoardView extends ViewKit.View {
  setupUI() {
    this.itemGap = 5
    this.edgeGap = 15
    this.itemWidth = 50
    this.maxItemWidth = 65
    this.bgColor = 'white'
    this.boardFlags = []
    this.winBlock = null
    this.startPosition = null
    // this.currentPosition = null
    this.positionPathArray = []
    this.justCalcPath = false
  }

  reset() {
    // this.currentPosition = this.startPosition
    this.goPosition(this.startPosition)
  }

  randomValidPassPath() {
    return this.passPath(this.tipPaths)
  }

  passPath (positionArr = this.positionPathArray) {
    let passPath = positionArr.map(x => {
      return {
        row: x.row,
        colume: x.colume
      }
    })
    return {
      passPath,
      row: this.row,
      colume: this.colume
    }
  }

  get currentPosition () {
    if (this.positionPathArray.length == 0) {
      return null
    }
    return this.positionPathArray[this.positionPathArray.length - 1]
  }

  set currentPosition (value) {
    if (value == this.currentPosition) {
      return
    }

    for (let index in this.positionPathArray) {
      if (this.positionPathArray[index] == value) {
        for (let i = parseInt(index) + 1; i < this.positionPathArray.length; i++) {
          if (this.positionPathArray[i].selectView) {
            this.positionPathArray[i].selectView.removeFromParent()
            this.positionPathArray[i].selectView = null
            this.positionPathArray[i].state = 1
          }
        }
        this.positionPathArray.length = parseInt(index) + 1
        return
      }
    }
    let lastValue = this.currentPosition
    this.positionPathArray.push(value)
    value.state = 3
    this.addBodyView(value, lastValue)
  }



  get dataModel() {
    return this._dataModel
  }

  set dataModel(value) {
    this._dataModel = value
    this.update(value)
  }

  get duckHead () {
    if (this._duckHead == null) {
      let v = null
      v = new DuckHead(ViewKit.Rect.rect(0, 0, this.itemWidth, this.itemWidth))
      v.asGameHead()
      this._duckHead = v
    }
    return this._duckHead
  }

  get duckTail () {
    if (this._duckTail == null) {
      let v = null
      v = new DuckTail(ViewKit.Rect.rect(0, 0, this.itemWidth, this.itemWidth))
      v.userInteraction = false
      this._duckTail = v
    }
    return this._duckTail
  }

  addBodyView(position, prePosition) {
    if (prePosition == null || position == null || prePosition.view == null || position.view == null) {
      return
    }
    let firstItemView = null
    let lastItemView = null
    if (position.view.x < prePosition.view.x || position.view.y < prePosition.view.y) {
      firstItemView = position.view
      lastItemView = prePosition.view
    } else {
      firstItemView = prePosition.view
      lastItemView = position.view
    }
    let width = (lastItemView.x + lastItemView.width) - firstItemView.x
    let height = (lastItemView.y + lastItemView.height) - firstItemView.y
    let x = firstItemView.x
    let y = firstItemView.y
    let rect = prePosition.view.rect
    let v = new ViewKit.View(ViewKit.Rect.rect(rect.x, rect.y, rect.width, rect.height))
    // let v = new ViewKit.View(ViewKit.Rect.rect(firstItemView.x, firstItemView.y, width - 10, height - 10))
    v.userInteraction = false
    v.cornerRadius = 10
    v.bgColor = this.themeColor
    this.addSubviewBelow(v, this.duckHead)
    //animation
    ViewKit.Animation.get(v)
    .to({x, y, width, height}, 50)
    .start()
    position.selectView = v
  }


  findValidPath2() {
      let pathArr = []

      let nextPosition = this.startPosition
      let arr = null

      let nextPositionEvn = []
      let arrEvn = []
      let iEvn = []
      let i = 0

      let saveEvn = (arr, i) => {
        arrEvn.push(arr)
        if (i != null) {
          iEvn.push(i)
        }
      }

      let popEvn = () => {
        arr = arrEvn.pop(arr)
        return iEvn.pop()
      }

      let state = 1

      label1: while(true) {
        this.counter += 1
        if (state == 0) {
          label2: for (; i < arr.length;) {
            nextPosition = arr[i]
            i += 1
            saveEvn(arr, i)
            state = 1
            continue label1
          }
          pathArr.pop()
          if (arrEvn.length == 0) {
            return null
          }
          i = popEvn()
          state = 0
        } else if (state == 1) {
          pathArr.push(nextPosition)
          if (pathArr.length == this.boardBlockCount) {
            return pathArr
          }

          arr = nextPosition.validNextStepNotBackArr(pathArr)

          for (let j = 0; j < arr.length; j++) {
            let tmpArr = pathArr.filter(x => x != nextPosition)
            let r = arr[j].validStepCount(tmpArr)
            if (r <= 1 && pathArr.length < (this.boardBlockCount - 1)) {
              pathArr.pop()
              if (arrEvn.length == 0) {
                return null
              }
              i = popEvn()
              state = 0
              continue label1
            }
          }

          if (arr.length == 0) {
            pathArr.pop()
            if (arrEvn.length == 0) {
              return null
            }
            i = popEvn()
          } else {
            i = 0
          }
          state = 0
        }
      }
  }

  findValidPath(pathArr = [], nextPosition = this.startPosition) {
    this.counter += 1
    pathArr.push(nextPosition)

    if (pathArr.length == this.boardBlockCount) {
      return pathArr
    }

    let arr = nextPosition.validNextStepNotBackArr(pathArr)
    if (arr.length == 0) {
      pathArr.pop()
      return null
    }

    for (let i = 0; i < arr.length; i ++) {
      let tmpArr = pathArr.filter(x => x != nextPosition)
      let r = arr[i].validStepCount(tmpArr)
       if (r <= 1 && pathArr.length < (this.boardBlockCount - 1)) {
         pathArr.pop()
        return null
       }
    }

    for (let i = 0; i < arr.length; i++) {
      let result = this.findValidPath(pathArr, arr[i])
      if (result) {
        return result
      }
    }

    pathArr.pop()
    return null
  }

  get boardFlagsFlat() {
    let r = []
    this.boardFlags.map(x => {
      r = r.concat(x)
    })
    return r
  }

  clear() {
    this.boardFlags.length = 0
  }

  update(model) {
    if (model == null) {
      return
    }
    this.clear()

    this.row = model.height
    this.colume = model.width

    this.itemWidth = (ViewKit.Device.share().screenWidth - ((model.width - 1) * this.itemGap)) / (model.width + 1)
    this.itemWidth = Math.min(this.itemWidth, this.maxItemWidth)

    let height = (ViewKit.Device.share().screenHeight * 0.6 - ((model.height - 1) * this.itemGap)) / model.height
    this.itemWidth = Math.min(this.itemWidth, height)

    this.width = model.width * (this.itemWidth + this.itemGap) - this.itemGap
    this.height = model.height * (this.itemWidth + this.itemGap) - this.itemGap

    for (let i = 0; i < model.height; i++) {
      let tmpArr = []
      for (let j = 0; j < model.width; j++) {
          let m = new BoardItemModel()
          m.row = i
          m.colume = j
          tmpArr.push(m)
      }
      this.boardFlags.push(tmpArr)
    }

    this.boardFlags.map(x => {
      x.map(a => {
        a.setupAroundRelation(this.boardFlags)
      })
    })

    let starting = this.itemWithLocation(this.location(model.starting))
    starting.state = 2
    this.startPosition = starting
    this.currentPosition = starting

    model.disappear.map(x => {
      let item = this.itemWithLocation(this.location(x))
      item.state = 0
    })
    this.boardBlockCount = model.width * model.height - model.disappear.length

    this.translateTipPath(model)
    if (this.justCalcPath == false) {
      this.drawBoard()
    }

    this.addTipImageView()
  }

  translateTipPath(model) {
      let r = []
      let position = this.startPosition
      r.push(position)
      let next = null
      model.route.map(direct => {
        if (position == null) {
          console.log('translateTipPath error');
          return
        }
        switch (direct) {
          case 0:
          next = position.right
          break;
          case 1:
          next = position.down
          break;
          case 2:
          next = position.left
          break;
          case 3:
          next = position.up
          break;
          default:
          break
        }
        r.push(next)
        position = next
      })
      return r
  }

  addTipImageView() {
    if (this.tipPaths == null) {
      this.counter = 0
      // let r = this.boardFlags[0].filter(x => x.state != 1)
      let time = Date.now()
      // this.tipPaths = this.findValidPath()
      // this.tipPaths = this.findValidPath2()
      this.tipPaths = this.translateTipPath(this.dataModel)
      let endTime = Date.now()
      console.log("counter:",this.counter, "time:", endTime - time);
    }

    if (this.justCalcPath) {
      return
    }

    if (this.tipPaths == null) {
      return
    }
    for (let i = 1; i < this.tipPaths.length; i++) {
      let direct = this.calcDirectWith(this.tipPaths[i].view, this.tipPaths[i - 1].view)
      this.tipPaths[i].view.tipDirect = direct
    }
    this.tipIndex = 1
  }

  goTipPath() {
    let min = Math.min(this.tipIndex, this.positionPathArray.length)
    console.log(min);
      for (let i = 0; i < min; i++) {
        if (this.tipPaths[i] === this.positionPathArray[i]) {
          continue
        } else {
          this.goPosition(this.positionPathArray[i - 1])
          return
        }
      }
      this.goPosition(this.positionPathArray[min - 1])
  }

  clickTip() {
    let step = 3
    let index = this.tipIndex
    this.goTipPath()
    for (let i = 0; i < step; i++) {
      let j = index + i
      if (j >= this.tipPaths.length) {
        return
      }
      // this.tipPaths[index + i].view.tipImageView.isHidden = false
      let tipView = this.tipPaths[j].view.tipImageView
      tipView.alpha = 0
      tipView.isHidden = false
      let time = 300
      ViewKit.Animation.get(tipView).wait(i * time)
      .to({'alpha': 1}, time)
      .start()
    }
    index += step
    if (index < this.tipPaths.length) {
      this.tipIndex = index
    }
  }

  drawBoard() {
    this.boardFlags.map((a, i) => {
      a.map((b, j) => {
        if (b.state == 0) {
          return
        }
        let v = new BoardBlockView(ViewKit.Rect.rect(j * (this.itemWidth + this.itemGap), i * (this.itemWidth + this.itemGap), this.itemWidth, this.itemWidth))
        b.view = v
        let tap = new ViewKit.TapGestureRecognzer(v, () => {
          // this.willGoToPosition(b)
        })

        v.tapStateBlock = (value) => {
          if (value) {
            this.willGoToPosition(b)
          } else {
            this.currentPosition.validNextStepNotBackArr(this.positionPathArray)
            .map(x => x.view.select(false))
          }
        }
        v.addGesture(tap)
        this.addSubview(v)
      })
    })
    this.duckHead.bgColor = this.themeColor
    if (this.originModel) {
      this.duckHead.updateModel(this.originModel)
      if (this.originModel.rgb.length == 4) {
        this.duckTail.tail.bgColor = this.originModel.rgb[0]
      }
    }

    this.addSubview(this.duckHead)
    this.addSubview(this.duckTail)
    this.duckHead.position = this.currentPosition.view.center
    this.duckTail.position = this.currentPosition.view.center
  }

  willGoToPosition(itemModel) {
    if (itemModel == null || itemModel.state == 0 || this.isWin()) {
      return
    }

    if (itemModel == this.currentPosition) {
      return
    }

    if (itemModel.state == 3) {
      this.goPosition(itemModel)
    }

    let isValidAction = this.currentPosition.validNextStepArr().reduce((r, x) => {
        r = r || (x === itemModel)
        return r
    }, false)

    if (!isValidAction && this.positionPathArray.indexOf(itemModel) == -1) {
      this.currentPosition.validNextStepNotBackArr(this.positionPathArray)
      .map(x => x.view.select(true, this.themeColor))
      return
    }

    this.goPosition(itemModel)
  }

  // 0 => down, 1 => left, 2 => top, 3 => right
  get direct () {
    return this._direct
  }

  set direct(value) {
    this._direct = value
    this.changeDuckDirect(value)
  }

  changeDuckDirect(value) {
    this.duckHead.rotate = value * (Math.PI / 2)
    if (this.positionPathArray.length <= 2) {
      this.duckTail.rotate = value * (Math.PI / 2)
    }
  }

  calcDirect() {
      let n_v = this.currentPosition.view
      let pre_index = this.positionPathArray.indexOf(this.currentPosition) - 1
      if (pre_index < 0) {
        this.direct = 0
        console.log('somethine wrong');
        return
      }
      let c_v = this.positionPathArray[pre_index].view
      this.direct = this.calcDirectWith(n_v, c_v)
  }

  calcDirectWith(postionView, prePositionView) {
    let n_v = postionView
    let c_v = prePositionView
    if (n_v.y == c_v.y) { //left of right
      if (n_v.x > c_v.x) { //right
        return 3
      } else {
        return 1
      }
    } else {
      if (n_v.y > c_v.y) {
        return 0
      } else {
        return 2
      }
    }

  }

  goPosition(itemModel) {
    ViewKit.SoundHelper.playSoundWithName('move')
    this.currentPosition.validNextStepArr()
    .map(x => x.view.select(false))
    this.currentPosition = itemModel
    this.calcDirect()
    this.duckHead.removeAnimation()
    ViewKit.Animation.get(this.duckHead)
    .to({x: itemModel.view.x, y: itemModel.view.y }, 50)
    .start()
    if (this.isWin() && this.winBlock) {
      setTimeout(this.winBlock, 500)
    }
  }

  isWin() {
    return this.positionPathArray.length == this.boardBlockCount
  }

  location(position) {
    if (position == 0) {
      return null
    }
    position -= 1
    let width = this.dataModel.width
    let height = this.dataModel.height
    let row = parseInt(position / width)
    let colume = position % width
    return [row, colume]
  }

  itemWithLocation(location){
    if (!location) {
      return null
    }
    let item = this.boardFlags[location[0]][location[1]]
    return item
  }

  addActionTarget() {
    let panGes = new ViewKit.PanGestureRecognzer(this, (ges) => {
      if (ges.state == 3) {
        this.currentPosition.validNextStepArr().map(x => {
          x.view.select(false)
        })
      } else {
        let p = this.pointInThisView(ges.currentPoint)
        this.scrollPoint(p)
      }

    })
    this.addGesture(panGes)

    // let tapGes = new ViewKit.TapGestureRecognzer(this, (ges) => {
    //   console.log('tap');
    //   let p = this.pointInThisView(ges.startPoint)
    //   this.scrollPoint(p)
    // })
    // this.addGesture(tapGes)
  }

  scrollPoint(point) {
    let position = this.positionWithPoint(point)
    this.willGoToPosition(position)
  }

  positionWithPoint(p) {
    if (p.x < 0 || p.y < 0 || p.x > this.width || p.y > this.height) {
      return null
    }

    let width = this.dataModel.width
    let height = this.dataModel.height

    let offset = this.itemWidth / (this.itemWidth + this.itemGap)
    let row = p.y / (this.itemWidth + this.itemGap)
    let rowInt = parseInt(row)
    if ((row - rowInt - offset) > 0) {
      return null
    }

    let colume = p.x / (this.itemWidth + this.itemGap)
    let columeInt = parseInt(colume)
    if ((colume - columeInt - offset) > 0) {
      return null
    }

    return this.boardFlags[rowInt][columeInt]
  }
}

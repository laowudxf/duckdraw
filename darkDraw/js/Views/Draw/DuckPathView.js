import * as ViewKit from '../../libs/ViewKit/index'

export default class DuckPathView extends ViewKit.View {

  setupUI() {
    this.userInteraction = false
  }

  draw(ctx, rect) {
    super.draw(ctx, rect)
    ctx.beginPath()
    ctx.save()
    ctx.strokeStyle = 'black'
    ctx.lineWidth = 200
    ctx.lineJoin = 'round';
    ctx.lineCap = 'round';
    ctx.moveTo(50, 50)
    ctx.lineTo(150, 50)
    ctx.stroke()
    ctx.restore()
  }
}

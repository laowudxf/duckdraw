
import * as ViewKit from '../../libs/ViewKit/index'
export default class BoardBlockView extends ViewKit.View {

  get tipImageView_arrow () {
    if (this._tipImageView_arrow == null) {
      let v = null
      v = new ViewKit.ImageView(ViewKit.getRes('arrow_white'), ViewKit.Rect.rect(0, 0, 27, 30))
      v.position = this.tipImageView.bounds.center
      this._tipImageView_arrow = v
    }
    return this._tipImageView_arrow
  }

  get tipImageView () {
    if (this._tipImageView == null) {
      let v = null
      v = new ViewKit.View(this.bounds)
      v.bgColor = '#faab34'
      v.cornerRadius = 10
      this._tipImageView = v
      this._tipImageView.isHidden = true
    }
    return this._tipImageView
  }

  setupUI() {
    this.bgColor = '#e5e5e5'
    this.cornerRadius = 10
    this._selected = false
    this.addSubview(this.tipImageView)
    this.tipImageView.addSubview(this.tipImageView_arrow)
  }

  get tipDirect () {
    return this._tipDirect
  }

  set tipDirect(value) {
    this._tipDirect = value
    this.tipImageView.rotate = value * Math.PI / 2
  }

  select(selected = true, color = null) {
    if (this._selected == selected) {
      return
    }
    this._selected = selected
    if (selected) {
      if (color) {
        this.bgColor = color
      }
      ViewKit.Animation.get(this)
      .to({'scale.x': 0.9, 'scale.y': 0.9}, 100)
      .start()
    } else {
      this.bgColor = '#e5e5e5'
      ViewKit.Animation.get(this)
      .to({'scale.x': 1, 'scale.y': 1}, 100)
      .start()
    }
  }
}

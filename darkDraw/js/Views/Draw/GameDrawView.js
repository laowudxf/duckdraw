import * as ViewKit from '../../libs/ViewKit/index'
import PassDuckItemPass from '../Pass/PassDuckItemPass'
import GameBoardView from './GameBoardView'
import PassCompleteView from '../Pass/PassCompleteView'
import PassGroupCompleteView from '../Pass/PassGroupCompleteView'
import Service from '../../Service/Service'
import DataBus from '../../DataBus'
import User from '../../DataModel/User'
import ShareHelper from '../../Helper/ShareHelper'
import RewardView from './RewardView'


export default class GameDrawView extends ViewKit.View {

  get passTitleLabel () {
    if (this._passTitleLabel == null) {
      let v = null
      v =  new ViewKit.Label(ViewKit.Rect.rect(0, 0, 100, 50))
      v.setFont(25)
      v.text = '第1关'
      v.position = new ViewKit.Point(this.width / 2, this.backBtn.center.y)
      v.textColor = 'black'
      v.textAlign = 'center'
      this._passTitleLabel = v
    }
    return this._passTitleLabel
  }

  get reduceCoinLabel () {
    if (this._reduceCoinLabel == null) {
      let v = null
      v = new ViewKit.Label(ViewKit.Rect.rect(0, 0, 50, 20))
      v.text = '-50金币'
      v.textAlign = 'center'
      v.textColor = 'red'
      v.position = this.coinBgView.rect.center
      v.alpha = 0
      this._reduceCoinLabel = v
    }
    return this._reduceCoinLabel
  }

  get coinBgView () {
    if (this._coinBgView == null) {
      let v = null
      v = new ViewKit.View(ViewKit.Rect.rect(0, 0, 110, 30))
      v.cornerRadius = 15
      v.borderWidth = 3
      v.borderColor = '#d6d6d6'
      v.anchorY = 0
      v.position = new ViewKit.Point(this.width / 2, this.passTitleLabel.rect.bottom + 20)
      v.bgColor = '#e5e5e5'
      this._coinBgView = v
    }
    return this._coinBgView
  }

  get coinImageView () {
    if (this._coinImageView == null) {
      let v = null
      v = new ViewKit.ImageView(ViewKit.getRes('coin'), ViewKit.Rect.rect(0 , 0, 20, 20))
      v.anchorX = 0
      v.position = new ViewKit.Point(3, this.coinBgView.height / 2)
      this._coinImageView = v
    }
    return this._coinImageView
  }

  get coinLabel () {
    if (this._coinLabel == null) {
      let v = null
      v = new ViewKit.Label(ViewKit.Rect.rect(0, 0, this.coinBgView.width - this.coinImageView.rect.right, this.coinBgView.height))
      v.anchorX = 0
      v.anchorY = 0
      v.position = new ViewKit.Point(this.coinImageView.rect.right + 5, 0)
      v.textColor = 'black'
      v.textAlign = 'left'
      this._coinLabel = v
    }
    return this._coinLabel
  }

  get coinAddBtn () {
    if (this._coinAddBtn == null) {
      let v = null
      v = new ViewKit.Button(ViewKit.Rect.rect(0, 0, 30, 30))
      v.anchorX = 1
      v.anchorY = 0
      v.bgImage = ViewKit.getRes('coin_add')
      v.label.text = ''
      v.position = new ViewKit.Point(this.coinBgView.width, 0)
      this._coinAddBtn = v
    }
    return this._coinAddBtn
  }


  get backBtn() {
    if(this._backBtn == null) {
      let y = 17
      if (ViewKit.Device.share().isPhoneX) {
         y += 10
      }
      this._backBtn = new ViewKit.Button(ViewKit.Rect.rect(0, y, 72, 41.5))
      this._backBtn.text = ''

      this._backBtn.bgImage = ViewKit.getRes('btn_back')
    }
    return this._backBtn
  }

  get gameBoardView () {
    if (this._gameBoardView == null) {
      let v = null
      v = new GameBoardView(ViewKit.Rect.rect(0, 0, 100, 100))
      v.position = this.center
      this._gameBoardView = v
    }
    return this._gameBoardView
  }

  get shareBtn () {
    if (this._shareBtn == null) {
      let v = null
      v = new ViewKit.Button(ViewKit.Rect.rect(0, 0, 60, 60))
      v.anchorY = 1
      v.anchorX = 1
      v.position = new ViewKit.Point(this.width / 2 - 7, this.height - 20)
      v.bgImage = ViewKit.getRes('btn_share')
      v.text = ''
      this._shareBtn = v
    }
    return this._shareBtn
  }

  get replayBtn () {
    if (this._replayBtn == null) {
      let v = null
      v = new ViewKit.Button(ViewKit.Rect.rect(0, 0, 60, 60))
      v.anchorY = 1
      v.anchorX = 1
      v.position = new ViewKit.Point(this.shareBtn.x - 14, this.height - 20)
      v.bgImage = ViewKit.getRes('btn_replay')
      v.text = ''
      this._replayBtn = v
    }
    return this._replayBtn
  }

  get tipBtn () {
    if (this._tipBtn == null) {
      let v = null
      v = new ViewKit.Button(ViewKit.Rect.rect(0, 0, 60, 60))
      v.anchorY = 1
      v.anchorX = 0
      v.position = new ViewKit.Point(this.width / 2 + 7, this.height - 20)
      v.bgImage = ViewKit.getRes('btn_tip')
      v.text = ''
      this._tipBtn = v
    }
    return this._tipBtn
  }

  get couldTip() {
    console.log(this.gameBoardView.tipIndex, this.gameBoardView.tipPaths.length);
    return this.gameBoardView.tipIndex < (this.gameBoardView.tipPaths.length - 2)
  }
  get tipFreeBtn () {
    if (this._tipFreeBtn == null) {
      let v = null
      v = new ViewKit.Button(ViewKit.Rect.rect(0, 0, 64, 60))
      v.anchorY = 1
      v.anchorX = 0
      v.position = new ViewKit.Point(this.tipBtn.rect.right + 14, this.height - 20)
      v.bgImage = ViewKit.getRes('btn_tip_free')
      v.text = ''
      this._tipFreeBtn = v
    }
    return this._tipFreeBtn
  }

  get awardBtn () {
    if (this._awardBtn == null) {
      let v = null
      v = new ViewKit.Button(ViewKit.Rect.rect(0, 0, 50, 53))
      v.anchorY = 0.5
      v.anchorX = 1
      v.position = new ViewKit.Point(this.width - 14, this.coinBgView.rect.center.y)
      v.bgImage = ViewKit.getRes('daily_reward')
      v.text = ''
      this._awardBtn = v
    }
    return this._awardBtn
  }

  get moreBtn () {
    if (this._moreBtn == null) {
      let v = null
      // v = new RandomJumpGameView()
      v = new ViewKit.Button(ViewKit.Rect.rect(0, 0, 32, 42))
      v.bgImage = ViewKit.getRes('moreGame')
      v.anchorX = 0
      v.anchorY = 0
      v.text = ''
      v.position = new ViewKit.Point(0, 135)
      this._moreBtn = v
    }
    return this._moreBtn
  }

  reset() {
    this.gameBoardView.reset()
  }

  subscribe(before, now, key, instance) {
    if (now != null) {
      this.coinLabel.text = now + ''
      // this.playBtn.isHidden = false
      // this.loaddingLabel.isHidden = true
      // this.menus.map(x => {
      //   x.isHidden = false
      // })
    }
  }

  willMoveToSuperview(view) {
    if(view == null) {
      User.share().removeObserver(this)
      DataBus.share().showOpenDataView(false)
      return
    }
    DataBus.share().showChaseView()
    User.share().registerObserver(this, 'coin')
  }

  setupUI() {
    DataBus.postMessage({
      method: 'refreshChaseInfo',
      param: {
        score: User.share().selfScore
      }
    })
      this.bgColor = 'white'
      let views = [this.backBtn, this.gameBoardView, this.shareBtn, this.replayBtn, this.awardBtn, this.tipBtn, this.tipFreeBtn, this.passTitleLabel, this.coinBgView, this.reduceCoinLabel,this.moreBtn]
      views.map(x => {
        this.addSubview(x)
      })

      views = [this.coinImageView, this.coinLabel, this.coinAddBtn]
      this.coinLabel.text = User.share().coin + ''
      views.map(x => {
        this.coinBgView.addSubview(x)
      })

      this.gameBoardView.winBlock = () => {
        if (this.winBlock) {
          this.winBlock()
        }
      }

      this.freeBtnAnimation()

      let timeInterval = 150
      let offset = 0.08
      let translationY = 15

      this.awardBtn.animationAnchorY = 1
      ViewKit.Animation.get(this.awardBtn, {loop: true})
      .to({'scale.x': 1 + offset, 'scale.y': 1 - offset}, timeInterval)
      .to({'scale.x': 1 - offset, 'scale.y': 1 + offset, 'translation.y': -translationY}, timeInterval * 2)
      .to({'scale.x': 1, 'scale.y': 1, 'translation.y': 0}, timeInterval)
      .to({'scale.x': 1 + offset, 'scale.y': 1 - offset}, timeInterval / 2)
      .to({'scale.x': 1, 'scale.y': 1}, timeInterval / 2)
      .wait(2000)
      .start()
    }

    freeBtnAnimation() {

      let timeInterval = 200
      let offset = 0.08
      let translationY = 15

      this.tipFreeBtn.animationAnchorY = 1
      ViewKit.Animation.get(this.tipFreeBtn, {loop: true})
      .to({'scale.x': 1 + offset, 'scale.y': 1 - offset}, timeInterval)
      .to({'scale.x': 1 - offset, 'scale.y': 1 + offset, 'translation.y': -translationY}, timeInterval * 2)
      .to({'scale.x': 1, 'scale.y': 1, 'translation.y': 0}, timeInterval)
      .to({'scale.x': 1 + offset, 'scale.y': 1 - offset}, timeInterval / 2)
      .to({'scale.x': 1, 'scale.y': 1}, timeInterval / 2)
      .wait(3000)
      .start()

    }

    coinReduceAnimation() {
      this.reduceCoinLabel.removeAnimation()
      this.reduceCoinLabel.alpha = 1
      this.reduceCoinLabel.translation.y = 0
      this.reduceCoinLabel.scale.x = 1
      this.reduceCoinLabel.scale.y = 1
      ViewKit.Animation.get(this.reduceCoinLabel)
      .to({'translation.y': 70, 'alpha': 0, 'scale.x': 1.3, 'scale.y': 1.3}, 800)
      .start()
    }

    addActionTarget() {
      this.replayBtn.clickedBlock = () => {
        wx.aldSendEvent("闯关页-重玩")
        this.reset()
      }

      this.shareBtn.clickedBlock = () => {
        wx.aldSendEvent("闯关页-求助")
        ShareHelper.share()
      }


    }

    get dataModel() {
      return this._dataModel
    }

    set dataModel(value) {
      this._dataModel = value
      this.update(value, this.index)
    }

    get index() {
      return this._index
    }

    set index(value) {
      this._index = value
      this.update(this.dataModel, value)
    }

    update(model, index) {
      if (model == null || index == null) {
        return
      }

      //设置passTitle
      let passIndex = DataBus.share().passIndexInt(model.id) + index + 1
      this.passTitleLabel.text = '第' + passIndex + '关'

      let passData =  JSON.parse(model.pass[index].content)
      this.passData = passData
    console.log(model);
    console.log(passData);
    if (model.rgb.length == 4) {
      this.gameBoardView.themeColor = model.desc == '' ? 'green' : model.rgb[3]
    } else {
      // this.gameBoardView.themeColor = model.desc == '' ? 'green' : model.desc
    }
    passData.pic = model.pic
    this.gameBoardView.originModel = model
    this.gameBoardView.dataModel = passData
    this.gameBoardView.position = this.center
  }

}

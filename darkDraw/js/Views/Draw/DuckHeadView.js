import * as ViewKit from '../../libs/ViewKit/index'

export default class DuckHeadView extends ViewKit.View {
  get duckHeadImageView () {
    if (this._duckHeadImageView == null) {
      let v = null
      v = new ViewKit.ImageView(null, ViewKit.Rect.rect(0, 0, this.width, 0.71 * this.width))
      v.anchorY = 1
      v.anchorX = 0
      v.position = new ViewKit.Point(0, this.height + 6)
      this._duckHeadImageView = v
    }
    return this._duckHeadImageView
  }

  get duckTail () {
    if (this._duckTail == null) {
      let v = null
      v = new ViewKit.ImageView(ViewKit.getRes('duckImage/tail'), ViewKit.Rect.rect(0, 0, 16, 16))
      this._duckTail = v
      v.position = new ViewKit.Point(this.width / 2, 5)
    }
    return this._duckTail
  }

  setupUI() {
    // this.rotate = Math.PI / 2
    // this.scale = new ViewKit.Point(1.5, 1.5)
    let views = [this.duckHeadImageView]
    views.map(x => {
      this.addSubview(x)
    })
    this.cornerRadius = 10

    this.direct = 'down'
    // ViewKit.Animation.get(this.duckTail, {loop: true})
    // .to({'height': 32}, 500)
    // .wait(500)
    // .start()
    //
    // ViewKit.Animation.get(this.duckTail, {loop: true})
    // .wait(500)
    // .to({'height': 16}, 500)
    // .start()
  }

  get direct () {
    return this._direct
  }

  set direct(value) {
    this._direct = value
  }

}
